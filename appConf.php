<?php
// Fichier contenant les contantes de paramétrage de notre application (URL, chemins, etc)

// On en profite pour paramétrer notre display_errors à 1 de manière à forcer l'affichage des erreurs si ce n'était pas le cas dans la config php.ini.
// Ne pas laisser en prod !
if (!ini_get('display_errors')) {
    ini_set('display_errors', '1');
}

define("URL", 'http://crud.dev');

define("BASE_URL", $_SERVER["DOCUMENT_ROOT"]);
