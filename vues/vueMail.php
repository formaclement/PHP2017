<?php

$template = '

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body style="background-color:#e3e3e3; font-family: Georgia, Times, serif; font-size: 16px; color: #444;">
    <table cellspacing="0" cellpadding="0" border="0" style="width: 650px; margin: auto;">
      <tr style="border-bottom: 1px solid #444; font-size: 14px;">
        <td>Crud.com</td>
        <td style="text-align:center;">Votre super site</td>
      </tr>
      <tr>
        <td colspan="2">
          '.$contenuMail.'
        </td>
      </tr>
      <tr>
        <td colspan="2" style="font-size: 14px;">Ceci est un mail automatique envoyé par le site crud.com</td>
      </tr>
    </table>
  </body>
</html>
';

/*
Pour débuger l'intégration HTML du mail :

$contenuMail = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
echo $template;

*/
