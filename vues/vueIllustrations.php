<?php
require("vues/headerAdmin.php");

// taille en Mo max de la photo
$tailleMax = 6;
?>

<fieldset>
  <legend>Ajouter une nouvelle illustration</legend>
  <?php
  if(!empty($_GET["mess"]) && $_GET["mess"] == "newIllus0") {
  ?>
  Attention, votre texte alternatif doit comporter au moins 20 caractères et votre image doit peser moins de 6Mo.
  <?php
  }
  ?>
  <?php if(!empty($page["erreur"])) echo $page["erreur"]; ?>
  <form class="" enctype="multipart/form-data" action="index.php?page=illustrations&action=create" method="post">
    <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $tailleMax * 1000000; ?>">
    <input type="text" name="texte" placeholder="Texte alternatif de l'image " value="<?php if(!empty($page["donnees"]["donnees"]["texte"])) echo $page["donnees"]["donnees"]["texte"]; ?>"/><br />
    <input type="file" name="photo" value="" /><br />
    <button type="" name="">Enregistrer</button>
  </form>
</fieldset>

<?php
if(!empty($page["illus"])) {
  echo "<ul>";
  while($illus = $page["illus"]->fetch()):
  ?>
    <li id="<?php echo $illus["id_illus"]; ?>">
      <form class="" action="index.php?page=illustrations&action=update&id=<?php echo $illus["id_illus"]; ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $tailleMax * 1000000; ?>">
        <input type="file" name="photo" value="">
        <img src="<?php theUrlIllustration($illus["url_illus"], "thumb"); ?>" alt="">
        <input type="text" name="texte" value="<?php echo $illus["texte_illus"]; ?>">
        <button type="" name="button">Modifier</button>
        <br />
        <a href="index.php?page=illustrations&action=delete&id=<?php echo $illus["id_illus"]; ?>">Supprimer cette image</a></p>
      </form>

    </li>
  <?php
  endwhile;
  echo "</ul>";
}
?>



<?php
require("vues/footer.php");
?>
