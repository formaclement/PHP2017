
<?php
// Chargement des scripts externes
?>
<script src="<?php echo URL; ?>/vendor/components/jquery/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<?php // Chargement de l'API Google Maps ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEIIcPNASDpGnSCBFZX7oOfo369JZNZyM&callback=initialisation"></script>
<script src="/assets/js/markerclusterer.js"></script>
<script src="<?php echo URL; ?>/assets/js/scripts.js"></script>
</body>
</html>
