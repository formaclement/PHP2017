<?php
require("vues/headerAdmin.php");
?>
    <hr />
    <a href="index.php?page=utilisateurs&action=export">Exporter la liste des utilisateurs en CSV</a>
    <hr />
    <a href='index.php?page=utilisateurs&action=create'>Créer un nouvel utilisateur</a><hr />
    <table>
      <tr>
          <th>NOM</th>
          <th>PRENOM</th>
          <th>actions</th>
      </tr>
      <?php while($utilisateur = $page["corps"]->fetch()): ?>
        <tr>
          <td><?php echo afficheImage($utilisateur["img2_utilisateur"]); ?></td>
          <td><?php echo $utilisateur["nom_utilisateur"]; ?></td>
          <td><?php echo $utilisateur["prenom_utilisateur"]; ?></td>
          <td>
            <a href='index.php?page=utilisateurs&action=delete&id=<?php echo $utilisateur["id_utilisateur"]; ?>'>supprimer</a><br />
            <a href='index.php?page=utilisateurs&action=update&id=<?php echo $utilisateur["id_utilisateur"]; ?>'>détails</a>
          </td>
        </tr>
      <?php endwhile; ?>
    </table>
<?php
require("vues/footer.php");
?>
