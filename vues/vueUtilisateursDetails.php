<?php
// Vue servant à afficher les détails d'un utilisateur à travers un formulaire.
require("vues/headerAdmin.php");
$donnees = $page["corps"]["donnees"];

// taille en Mo max de la photo
$tailleMax = 6;

?>
<?php if(!empty($page["corps"]["erreur"])) echo $page["corps"]["erreur"]; ?>
<form class="" action="" method="post" enctype="multipart/form-data">
  <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $tailleMax*1000000; ?>">
  <input type="hidden" name="id" value="<?php echo $donnees["id"]; ?>"><br />
  <input type="text" name="nom" value="<?php echo $donnees["nom"]; ?>" placeholder="nom"><br />
  <input type="text" name="prenom" value="<?php echo $donnees["prenom"]; ?>" placeholder="prénom"><br />
  <input type="text" name="email" value="<?php echo $donnees["email"]; ?>" placeholder="Email"><br />
  <input type="text" name="login" value="<?php echo $donnees["login"]; ?>" placeholder="login"><br />
  <input type="text" name="age" value="<?php echo $donnees["age"]; ?>" placeholder="age"><br />
  <input type="text" name="classe" value="<?php echo $donnees["classe"]; ?>" placeholder="classe"><br>
  <br />
  <?php
  if($_GET["action"] == 'create') {
  ?>
  <input type="password" name="pass" value="" placeholder="mot de passe"><br>
  <?php
  }
  ?>
  <label for="photo">Photo de profil : </label><br />
  <?php
    if(!empty($donnees["photoVignette"])) {
      echo afficheImage($donnees["photoVignette"]);
      echo "<br />";
      echo "<a href='index.php?page=utilisateurs&action=suppPhoto&id=".$donnees["id"]."'>Supprimer l'image de profil</a>";
    }
  ?><br />
  <input type="file" name="photo" id="photo" value="" />
  <br />
  <hr />
  Localisation :<br />
  <input type="hidden" name="lat" value="<?php echo $donnees["lat"]; ?>" id="inputLat">
  <input type="hidden" name="lng" value="<?php echo $donnees["lng"]; ?>" id="inputLng">
  <p id="geocodeInfos">
    <input type="text" name="adresse" value="<?php echo $donnees["adresse"]; ?>" placeholder="Adresse (ex: 1 Place Bellecour)" id="adresse"><br />
    <input type="text" name="cp" value="<?php echo $donnees["cp"]; ?>" placeholder="Code postal" id="cp"><br />
    <input type="text" name="ville" value="<?php echo $donnees["ville"]; ?>" placeholder="Ville" id="ville">
    <span class="btn">Placer mon adresse !</span>
  </p>
  <hr />
  <div id="mapDiv"></div>

  <button>Enregistrer</button>
</form>
<hr />
<a href="index.php?page=utilisateurs&action=updatePass&id=<?php echo $donnees["id"]; ?>">Modifier le mot de passe</a><br />
<hr />
<a href='index.php?page=utilisateurs&action=delete&id=<?php echo $donnees["id"]; ?>'>supprimer l'utilisateur</a>

<?php
require("vues/footer.php");
?>
