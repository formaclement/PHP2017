<?php
// On utilise la méthode GET pour utiliser l'URL pour passer les paramètres de recherche.
// De cette manière on peut accéder directement aux résultats via un lien, ce qui permet de rendre accessibles ces résultats aux moteurs de recherche
?>
<form class="" action="" method="GET" id="search">
  <h4>Rechercher parmi les actualités</h4>
  <input type="hidden" name="page" value="recherche" />
  <input type="text" name="search" value="<?php if(isset($_GET["search"])) echo htmlspecialchars($_GET["search"]); ?>" id="searchValue">
  <button type="submit">OK</button>
</form>
