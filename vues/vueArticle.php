<?php
require_once("header.php");

$format = new IntlDateFormatter("fr_FR", IntlDateFormatter::FULL, IntlDateFormatter::SHORT);
$datePublication = strtotime($page["date_creation"]);
$dateModif = strtotime($page["date_modif"]);

// Affichage d'une fiche article
echo "<p>".$page["texte_actu"]."</p>";
// Affichage des illustrations
foreach ($listeIllus as $illustration) {
?>
<a href="<?php theUrlIllustration($illustration['url_illus'], "large") ?>">
  <img src="<?php theUrlIllustration($illustration['url_illus'], "thumb") ?>" alt="<?php echo $illustration["texte_illus"]; ?>" />
</a>
<?php
}
if(!empty($page["nom_utilisateur"])) {
  echo "<hr />Auteur :
  <a href='".URL."/auteur/".$page["login_utilisateur"]."' class='ajaxAuteur'> ".ucwords($page["nom_utilisateur"])." ".ucwords($page["prenom_utilisateur"])." </a>";
}
echo "<hr />";
echo "Date de publication : ".$format->format($datePublication)." | Dernière modification : ".$format->format($dateModif);

require_once("footer.php");
 ?>
