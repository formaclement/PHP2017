<?php
require("vues/headerAdmin.php");
?>
<div class="listeActus">
<a href="index.php?page=actualites&action=create">Ajouter une nouvelle actualité</a>

<?php
while($actu = $page["corps"]->fetch()): ?>
<ul>
  <li>
    <strong><?php echo $actu["titre_actu"]; ?></strong>
    <p><?php echo $actu["texte_actu"]; ?></p>
    <hr />
    <p>
    <?php
    if(!empty($actu["prenom_utilisateur"] && !empty($actu["nom_utilisateur"]))) {

      echo "<a href='index.php?page=utilisateurs&action=update&id=".$actu["id_utilisateur"]."'>Auteur : ".$actu["prenom_utilisateur"]." ".$actu["nom_utilisateur"]."</a>";
    } else {
      echo "L'auteur n'est pas disponible.";
    }
    ?>
     //
     Catégorie :
     <?php
     echo $actu["nom_cat"];
     ?>
    </p>
    <a href="index.php?page=actualites&action=update&id=<?php echo $actu["id_actu"]; ?>">Modifier l'actualité</a> | <a href="index.php?page=actualites&action=delete&id=<?php echo $actu["id_actu"]; ?>">Supprimer l'actualité</a>
  </li>
</ul>
<?php
endwhile;
require("vues/footer.php");
?>
</div>
<aside class="listeCats">
  <h3>Gestion des catégories</h3>
  <ul>
  <?php
  while($cat = $listeCats["donnees"]->fetch()): ?>
    <li>
      <?php echo $cat["nom_cat"]; ?> | <a href="index.php?page=categories&action=update&id=<?php echo $cat["id_cat"]; ?>" data-id="<?php echo $cat["id_cat"]; ?>" class="modifCat">Modifier</a> | <a href="index.php?page=categories&action=delete&id=<?php echo $cat['id_cat']; ?>" class='deleteCat'>Supprimer</a>
    </li>
  <?php endwhile; ?>
</ul>
  <a href="index.php?page=categories&action=create" id="createCat">Ajouter une nouvelle catégorie</a>

</aside>
