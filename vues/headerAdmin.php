<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $page["titre"]; ?></title>
    <link rel="stylesheet" href="assets/css/styles.css">
  </head>
  <body class="admin">
    <div class="">
      Bienvenue <?php
      echo $user["id"]." : ".$user["nom"]." ".$user["prenom"]." - ".$user["roleFormat"]." (". $user["ageFormat"].", classe : ".$user["classe"].")";

      ?>
      <a href="index.php?page=connexion&action=deco">Deconnexion</a> | <a href="index.php?page=panel">Accueil</a>
    </div>
    <h1><?php echo $page["titre"]; ?></h1>
    <?php echo $message; ?>
