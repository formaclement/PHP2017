<?php
require_once("headerAdmin.php");

$donnees = $page["corps"]["donnees"];
?>
<hr />
<a href="index.php?page=actualites&action=generationPDF&id=<?php echo $donnees["id_actu"]; ?>">Générer un PDF de cette actualité</a>
<hr />
<?php if(!empty($page["corps"]["erreur"])) echo $page["corps"]["erreur"]; ?>
<form class="" action="" method="post">
  <input type="hidden" name="id" value="<?php echo $donnees["id_actu"]; ?>"><br />
  <input type="text" name="titre_actu" value="<?php echo $donnees["titre_actu"]; ?>" placeholder="titre"><br />
  <textarea name="texte_actu" rows="8" cols="80"><?php echo $donnees["texte_actu"]; ?></textarea><br />
  <select class="" name="id_utilisateur">
    <?php while($utilisateur = $utilisateurs["donnees"]->fetch()): ?>
      <option value="<?php echo $utilisateur["id_utilisateur"]; ?>" <?php if($donnees["id_utilisateur"] == $utilisateur["id_utilisateur"]) echo "selected" ?> >
        <?php echo $utilisateur["prenom_utilisateur"]." ".$utilisateur["nom_utilisateur"]; ?>
      </option>

    <?php endwhile; ?>
  </select>
  <br />

  <button>Enregistrer</button>
</form>
<hr />
<?php
$listeIdIllus = array();

if(!empty($listeIllus)) {
  echo "<h3>Liste des illustrations présentes</h3>";
  foreach ($listeIllus as $illustration) {
    array_push($listeIdIllus, $illustration["id_illus"]);
    ?>
    <a href="index.php?page=IllusActus&action=delete&idActu=<?php echo $donnees["id_actu"]; ?>&idIllus=<?php echo $illustration['id_illus']; ?>">Supprimer cette illustration</a><br />
    <img src="<?php theUrlIllustration($illustration['url_illus'], "thumb") ?>" alt="<?php echo $illustration["texte_illus"]; ?>" /><br />
    <?php
  }
}
?>
<hr />
<a href='index.php?page=actualites&action=delete&id=<?php echo $donnees["id_actu"]; ?>'>supprimer l'actualité</a>

<div id="liste_illus">
<h3>Médiathèque</h3>
<?php
while($illus = $mediatheque["illus"]->fetch()):
  if(!in_array($illus["id_illus"], $listeIdIllus)) {
  ?>
  <a href="index.php?page=IllusActus&action=create&idActu=<?php echo $donnees["id_actu"]; ?>&idIllus=<?php echo $illus['id_illus']; ?>">ajouter<br />
  <img src="<?php theUrlIllustration($illus["url_illus"], "thumb"); ?>" alt=""></a>
  <?php
  }
endwhile;
?>

</div>
<?php
require_once("footer.php");
 ?>
