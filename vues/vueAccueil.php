<?php
require("vues/header.php");
//var_dump($page);
?>

<h2>Actualités</h2>

<?php
// On récupère la vue affichant le moteur de recherche
include("vues/vueMoteur.php");
?>

<?php while($actu = $page["donnees"]->fetch()): ?>

<ul>
  <li>
    <?php echo afficheImage($actu["img2_utilisateur"]);  ?>
    <strong><a href="<?php echo getLienActualite($actu["titre_actu"], $actu["id_actu"]); ?>"><?php echo $actu["titre_actu"]; ?></a></strong>
    <p><?php echo $actu["texte_actu"]; ?></p>
    <hr />
    <p>
    <?php
    if(!empty($actu["prenom_utilisateur"] && !empty($actu["nom_utilisateur"]))) {
      echo "Auteur : ".$actu["prenom_utilisateur"]." ".$actu["nom_utilisateur"];
    } else {
      echo "L'auteur n'est pas disponible.";
    }
    ?>
    </p>
  </li>
</ul>
<?php
endwhile;
?>
<h2>Carte de nos utilisateurs</h2>
<div id="mapDiv"></div>
<div id="bulle">
  <span>Fermer</span>
  <div class="content"></div>
</div>
<div class="listeUtilisateurs">
<?php
while($utilisateur = $listeUtilisateurs["donnees"]->fetch()):
?>
  <div class="utilisateur">
    <span class="nom"><?php echo $utilisateur["nom_utilisateur"]; ?></span>
    <span class="prenom"><?php echo $utilisateur["prenom_utilisateur"]; ?></span>
    <span class="adresse"><?php echo $utilisateur["adresse_utilisateur"]." ".$utilisateur["cp_utilisateur"]." ".$utilisateur["ville_utilisateur"] ?></span>
    <span class="lat"><?php echo $utilisateur["lat_utilisateur"]; ?></span>
    <span class="lng"><?php echo $utilisateur["lng_utilisateur"]; ?></span>
  </div>
<?php
endwhile;
?>
</div>
<hr />
<h4>Actualités du blog</h4>
<?php
// affichage du flux RSS
echo $rss;
?>

<?php
require("vues/footer.php");
?>
