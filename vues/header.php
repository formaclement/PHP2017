<?php
// Gestion du cookie de première visite

// si l'élément "visite" de la superglobale $_COOKIE existe et est égale à "OK"
if(isset($_COOKIE["visite"]) && $_COOKIE["visite"] == "OK") {
  // la variable $messageBienvenue prend la valeur false
  $messageBienvenue = false;
} else { // sinon
  // on crée un nouveau cookie appelé visite, prenant la valeur "OK", dont la date d'expiration est fixée à une année approximativement.
  setcookie("visite", "OK", time()+60*60*24*30*12);
  // la variable $messageBienvenue prend la valeur true
  $messageBienvenue = true;
}

// Gestion du cookie de changement de thèmes

// si on a un élément "theme" dans notre superglobale $_COOKIE
if(isset($_COOKIE["theme"])) {
  // on stocke sa valeur dans une variable $theme
  $theme = $_COOKIE["theme"];
} else {
  // sinon on stocke la valeur "ete" dans une variable $theme, ete est donc la valeur par défaut.
  $theme = "ete";
}

// si on a un élément theme passé en $_GET
if(isset($_GET["theme"])) {
  // si $_GET["theme"] est égal à hiver
  if($_GET["theme"] == "hiver") {
    // on crée un nouveau cookie appelé theme, dont la valeur est hiver et une date d'expiration d'environ un an
    setcookie("theme", "hiver", time()+60*60*24*30*12);
    // $theme prend la valeur "hiver"
    $theme = "hiver";
  } else {
    // sinon on crée un cookie theme de valeur "ete", dont la date d'expiration est d'environ un an
    setcookie("theme", "ete", time()+60*60*24*30*12);
    // $theme prend la valeur "ete"
    $theme = "ete";
  }
}

/*
Cette fonction affiche le thème qui n'est pas actif actuellement.
*/
// on crée une fonction getAutreTheme prenant pour paramètre une chaîne $theme
function getAutreTheme($theme) {
  // On crée un tableau $themes contenant deux valeurs : "ete" et "hiver"
  $themes = array(
    "ete", "hiver"
  );
  // on boucle sur le tableau $themes
  foreach ($themes as $letheme) {
    // si la valeur passée en paramètre dans $theme est différente de la valeur du tour de boucle dans le tableau
    if($letheme != $theme) {
      // on affiche la valeur du tour de boucle
      echo $letheme;
    }
  }
}



?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $page["titre"]; ?></title>
    <link rel="stylesheet" href="<?php echo URL; ?>/assets/css/styles.css">
    <?php // On charge la feuille de style du thème utilisé actuellement, qui surcharge la feuille de style de base. ?>
    <link rel="stylesheet" href="<?php echo URL; ?>/assets/css/styles-<?php echo $theme; ?>.css">
  </head>
  <body>
    <nav>
      <ul>
        <li><a href="<?php echo URL; ?>">Accueil</a></li>
      </ul>
    </nav>
    <nav>
      <?php // On crée un lien permettant de changer de thème à l'aide de la valeur de $_GET["theme"] ?>
      <a href="<?php echo URL; ?>/index.php?theme=<?php getAutreTheme($theme); ?>">Passer au thème d'<?php getAutreTheme($theme); ?> !</a>
    </nav>
    <hr />
    <h1><?php echo $page["titre"]; ?></h1>
    <?php echo $message; ?>
    <?php
    // si $messageBienvenue est égale à "true" on affiche un message de bienvenue : on est dans le cas d'une première visite sur le site puisque le cookie dédié n'existait pas dans le navigateur
    if($messageBienvenue == true) {
    ?>
    <hr/>
    Bienvenue sur le site de démonstration !<br />
    Vous pourrez y voir plein d'exemple de ce qu'on peut faire avec PHP, MySQL et un peu de JS !
    <hr />
    <?php
  } else {
    // on n'est pas dans le cas d'une première visite, on affiche le message "heureux de vous revoir"
    ?>
    <hr />
    Heureux de vous revoir !
    Vous connaissez déjà notre site :)
    <hr />
    <?php
  }
    ?>
