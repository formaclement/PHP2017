<?php

function ditBonjour($prenom="") {
  if(empty($prenom)) {
    throw new Exception ("Le prénom ne doit pas être vide !");
  } else if (!is_string($prenom)) {
    throw new Exception ("Le prénom doit être une chaîne de caractères !");
  }
  return "Bonjour ".$prenom;
}

try {
  echo ditBonjour();
} catch (Exception $e) {
  // echo $e->getMessage();
  throw new Exception("$e->getMessage()");
}

 ?>
