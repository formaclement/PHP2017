<?php
// Le modèle est le composant MVC chargé de s'occuper de la logique métier de notre application.
// C'est dans le modèle qu'on va trouver les fonctions de récupération de données dans notre base.
// C'est aussi ici qu'on va traiter ces données avant enregistrement ou affichage par transmission vers la vue.
// On charge le modele de connexion à la BDD avec require_once pour éviter les erreurs dues à la redéclaration de fonctions.
require_once("modeleBdd.php");
require_once("modeleSession.php");
require_once("helpersForm.php");
require_once("modelePhoto.php");

/*
function getUtilisateurs
Fonction retournant la liste simplifiée des utilisateurs.
*/
function getUtilisateurs() {
  $db = connect();
  // Code de récupération de notre liste d'utilisateurs
  // 1 - écriture de la requête SQL SELECT.
  $sql = "SELECT id_utilisateur, nom_utilisateur, prenom_utilisateur, img2_utilisateur FROM crud_utilisateur";
  // 2 - Envoi de la requête avec la méthode try catch
  try {
    $retour["donnees"] = $db->query($sql);
    $retour["statut"] = "ok";
  } catch (PDOException $erreur) {
    $retour["donnees"] = $erreur->getMessage();
    $retour["statut"] = "erreur";
  }
  return $retour;
}


function getUtilisateursGeoloc() {
  $db = connect();
  // Code de récupération de notre liste d'utilisateurs
  // 1 - écriture de la requête SQL SELECT.
  $sql = "SELECT id_utilisateur, nom_utilisateur, prenom_utilisateur, lat_utilisateur, lng_utilisateur, adresse_utilisateur, ville_utilisateur, cp_utilisateur FROM crud_utilisateur";
  // 2 - Envoi de la requête avec la méthode try catch
  try {
    $retour["donnees"] = $db->query($sql);
    $retour["statut"] = "ok";
  } catch (PDOException $erreur) {
    $retour["donnees"] = $erreur->getMessage();
    $retour["statut"] = "erreur";
  }
  return $retour;
}

function getUtilisateurByLogin($login) {
  $db = connect();
  $sql = "SELECT id_utilisateur, prenom_utilisateur, nom_utilisateur, pass_utilisateur, age_utilisateur, classe_utilisateur, role_utilisateur, email_utilisateur FROM crud_utilisateur WHERE login_utilisateur = :login";
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':login', $login, PDO::PARAM_STR);
    // Exécution de la requête
    $req->execute();

    $utilisateur = $req->fetchAll();
    return $utilisateur;
  } catch (PDOException $erreur) {
    // si la requête PDO échoue, on attrape l'exception de type PDOException et on envoie une nouvelle exception en reprenant le même message.
    throw new Exception ($erreur->getMessage());
  }
}

/*
Function getImageUtilisateurByLogin()
Retourne la photo de profil de l'utilisateur suivant le login et la taille passés en paramères.
*/
function getImageUtilisateurByLogin($login, $taille = "thumb") {
  $db = connect();
  $sql = "SELECT img1_utilisateur, img2_utilisateur FROM crud_utilisateur WHERE login_utilisateur = :login";
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':login', $login, PDO::PARAM_STR);
    // Exécution de la requête
    $req->execute();
    $resultImg = $req->fetchAll()[0];

    $imgLarge = $resultImg["img1_utilisateur"];
    $imgThumb = $resultImg["img2_utilisateur"];
    if($taille == "thumb") {
      return afficheImage($imgThumb);
    } else if($taille == "large") {
      return afficheImage($imgLarge);
    } else {
      throw new Exception("Paramètre invalide !");
    }

  } catch (PDOException $erreur) {
    // si la requête PDO échoue, on attrape l'exception de type PDOException et on envoie une nouvelle exception en reprenant le même message.
    throw new Exception ($erreur->getMessage());
  }
}

/*
function afficheImage() : Fonction permettant de vérifier si le fichier existe et retourne la balise IMG.
@param string $fichier : l'URI du fichier à afficher
*/
function afficheImage($fichier) {
  if(file_exists(BASE_URL."/uploads/".$fichier) && !empty($fichier)) {
    return "<img src='"."/uploads/".$fichier."' />";
  } else {
    return "Fichier de photo inexistant !";
  }
}

/*
Function connexion
Gere la connexion d'un membre via le formulaire.
*/
function connexion() {
  $contenu["titre"] = "Connexion à l'administration";
  $contenu["erreur"] = "";

  if($_SERVER["REQUEST_METHOD"] == "POST") {
    $contenu["login"] = htmlspecialchars($_POST["login"]);
    // on vient de soumettre le formulaire, on le teste
    if(($login = verifTexte($_POST["login"])) && ($pass = verifTexte($_POST["pass"]))) {

      try {
        // on récupère les données de l'utilisateur en base avec la fonction getUtilisateurByLogin($login)
        $utilisateur = getUtilisateurByLogin($login);        // si $utilisateur est vide, l'identifiant n'existe pas en base
        if(empty($utilisateur)) {
          $contenu["erreur"] = "Problème dans vos données de connexion.";
        } else {
          // on a l'identifiant en base, on va donc analyser le mot de passe.
          $passBase = $utilisateur[0]["pass_utilisateur"];
          if(password_verify($pass, $passBase)) {
            // On remplit ses données de session
            $_SESSION["log"] = "OK";
            $_SESSION["token"] = generationToken();
            $_SESSION["utilisateur"] = array(
              "id" => $utilisateur[0]["id_utilisateur"],
              "nom" => $utilisateur[0]["nom_utilisateur"],
              "prenom" => $utilisateur[0]["prenom_utilisateur"],
              "age" => $utilisateur[0]["age_utilisateur"],
              "classe" => $utilisateur[0]["classe_utilisateur"],
              "role" => $utilisateur[0]["role_utilisateur"]
            );

            if($_SESSION["utilisateur"]["role"] == 0) {
              // redirection vers la fiche du membre
              header("location:index.php?page=utilisateurs&action=update&id=".$_SESSION["utilisateur"]["id"]);
              exit;
            } else {
              // redirection vers le tableau de bord de l'admin
              header("location:index.php?page=panel");
              exit;
            }
          } else {
            $contenu["erreur"] = "Problème dans vos données de connexion";
          }

        }
      } catch (Exception $e) {
        echo $e->getMessage();
      }

    } else {
      $contenu["erreur"] = "Problème dans vos données de connexion.";
    }
  }
  return $contenu;
}


/*
function listeUtilisateurs
Permet de retourner la liste simplifiée des utilisateurs ou le code d'erreur
*/
function listeUtilisateurs() {
  $contenu = array();
  $contenu["titre"] = "Liste des utilisateurs du site.";
  $contenu["corps"] = "";
  $donnees = getUtilisateurs();
  if($donnees["statut"] == "ok") {
    $contenu["corps"] = $donnees["donnees"];
  } else {
    throw new Exception ($donnees["donnees"]);
    // $contenu["corps"].="<p class='erreur'>".$donnees["donnees"]."</p>";
  }
  return $contenu;
}

/*
Fonction saveUtilisateur
@param array $donnees : tableau des données relatives à l'utilisateur
@param string $mode : le mode d'utilisation de la fonction : en insert ou en update
Cette fonction a pour rôle d'enregistrer un utilisateur dans la BDD, que ce soit en insert ou en update
*/
function saveUtilisateur($donnees, $mode) {
  // on commence par instancier une nouvelle connexion PDO
  $db = connect();

  // On teste si le tableau $donnees n'est pas conforme à nos attentes (s'il est vide, ou si ce n'est pas un tableau), si c'est le cas, on envoie une nouvelle exception.
  if(empty($donnees) || !is_array($donnees)) {
    throw new Exception("Les données à enregistrer ne sont pas valides !");
  }
  // On teste le deuxième paramètre de notre fonction ($mode), qui doit avoir deux valeurs possibles : "insert" ou "update".
  // En fonction de ces valeurs, on va écrire une requête SQL différente.
  if($mode == "insert") {
    $role = "0";
    // requete SQL d'insertion
    $sql = "INSERT INTO crud_utilisateur VALUES (:id, :nom, :prenom, :login, :pass, :age, :classe, :role, :email, :lat, :lng, :adresse, :ville, :cp, :image1, :image2)";
  } else if ($mode == "update") {

    $sql = "UPDATE crud_utilisateur
    SET nom_utilisateur = :nom, prenom_utilisateur = :prenom, login_utilisateur = :login, age_utilisateur = :age, classe_utilisateur = :classe, email_utilisateur = :email, lat_utilisateur = :lat, lng_utilisateur = :lng, adresse_utilisateur = :adresse,  ville_utilisateur = :ville, cp_utilisateur = :cp ";
    if(!empty($donnees["photo"])) { // On teste si le champ photo est vide ou pas
      $sql .= ", img1_utilisateur = :image1, img2_utilisateur = :image2 ";
    }
    $sql .= "WHERE id_utilisateur = :id";


  } else {
    // si la valeur de $mode n'est pas conforme, on envoie une nouvelle exception.
    throw new Exception ("le mode d'utilisation de la fonction n'est pas reconnu !");
  }

  // On utilise un try catch pour lancer la requête PDO.
  try {
    if($donnees["id"] == "NULL") {
      $donnees["id"] = null;
    }
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':nom', $donnees["nom"], PDO::PARAM_STR);
    $req->bindParam(':prenom', $donnees["prenom"], PDO::PARAM_STR);
    $req->bindParam(':login', $donnees["login"], PDO::PARAM_STR);
    $req->bindParam(':age', $donnees["age"], PDO::PARAM_INT);
    $req->bindParam(':classe', $donnees["classe"], PDO::PARAM_STR);
    $req->bindParam(':id', $donnees["id"]);
    if ($mode == "insert") { $req->bindParam(':role', $role); }
    $req->bindParam(':email', $donnees["email"]);
    $req->bindParam(':lat', $donnees["lat"]);
    $req->bindParam(':lng', $donnees["lng"]);
    $req->bindParam(':adresse', $donnees["adresse"]);
    $req->bindParam(':cp', $donnees["cp"]);
    $req->bindParam(':ville', $donnees["ville"]);
    if(!empty($donnees["photo"])) {
      $req->bindParam(":image1", $donnees["photo"]);
      $req->bindParam(":image2", $donnees["photo2"]);
    }

    if($_GET["action"] == "create") {
      $req->bindParam(':pass', $donnees["pass"]);
    }

    // Exécution de la requête
    $req->execute();
  } catch (PDOException $erreur) {
    // si la requête PDO échoue, on attrape l'exception de type PDOException et on envoie une nouvelle exception en reprenant le même message.
    throw new Exception (print_r($donnees)."<br />".$erreur->getMessage());
  }
}


/*
Fonction createUtilisateur()
Cette fonction permet de gérer les infos relatives à la page de création d'un nouvel utilisateur
*/
function createUtilisateur() {
  if($_SESSION["utilisateur"]["role"] != 9) {
    header("location:index.php?page=utilisateurs");
    exit;
  }
  // On définit le titre de la page
  $retour["titre"] = "Création d'un nouvel utilisateur";

  // Définition des variables des données du formulaire (par défaut, tout est vide)
  $donnees["donnees"]["id"] = "NULL";
  $donnees["donnees"]["nom"] = "";
  $donnees["donnees"]["prenom"] = "";
  $donnees["donnees"]["login"] = "";
  $donnees["donnees"]["age"] = "";
  $donnees["donnees"]["classe"] = "";
  $donnees["donnees"]["email"] = "";
  $donnees["donnees"]["lat"] = "";
  $donnees["donnees"]["lng"] = "";
  $donnees["donnees"]["adresse"] = "";
  $donnees["donnees"]["cp"] = "";
  $donnees["donnees"]["ville"] = "";
  $donnees["donnees"]["photo"] = "";

  // Si on vient de soumettre le formulaire, on passe dans la condition suivante
  if($_SERVER["REQUEST_METHOD"] == "POST") {
    // On appelle la fonction verifFormulaire, qui va analyser les données transmises en POST et qui retourne un tableau composé de deux sous-éléments :
    // - les erreurs sous la forme d'un sous tableau
    // - Les données validées sous la forme d'un sous tableau
    $donnees = verifFormulaire();
    // si le tableau d'erreur est vide, c'est que le formulaire est validé
    if(empty($donnees["erreur"])) {
      // On utilise une structure try catch pour lancer notre requête d'insertion.
      try {

        // On enregistre la photo dans le dossier uploads en la redimensionnant avec la classe Claviska.
        // On récupère le uniqid utilisé pour le nom des photos dans la variable $idPhoto
        // Si tout se passe bien, on enchaîne sur l'enregistrement SQL de l'utilisateur.
        if($idPhoto = savePhoto($donnees["donnees"]["photo"]["ext"], "photo", $donnees["donnees"]["photo"]["mime"], "utilisateur")) {
          // On réécrit les uri des photos qu'on vient d'uploader, et on les ajoute à notre tableau $donnees["donnees"] pour les transmettre à notre fonction saveUtilisateur qui va les enregistrer en base.
          $donnees["donnees"]["photo2"] = $idPhoto."-thumb".$donnees["donnees"]["photo"]["ext"];
          $donnees["donnees"]["photo"] = $idPhoto."-large".$donnees["donnees"]["photo"]["ext"];
          // on appelle la fonction saveUtilisateur avec comme paramètres notre tableau de données renvoyé par la fonction verifFormulaire et avec le paramètre insert
          saveUtilisateur($donnees["donnees"], "insert");
          //si l'insertion s'est bien effectuée, on redirige vers la page de listing des utilisateurs
          header("location:index.php?page=utilisateurs&mess=insert1");

        } else {
          $donnees["erreur"] = "Un problème est survenu dans l'enregistrement de la photo, merci d'avertir un administrateur.";
        }
      } catch(Exception $e) {
        // s'il y a eu un problème dans l'insertion on attrape l'exception et on en renvoie une nouvelle avec le même message à notre fonction appelante (dans le contrôleur).
        throw new Exception ($e->getMessage());
      }

    } else {
      // si on a des erreurs dans notre vérification du formulaire, on transforme le sous-tableau en chaîne avec implode.
      $donnees["erreur"] = implode("<br />", $donnees["erreur"]);
      $donnees["erreur"] = "<p class='erreur'>".$donnees["erreur"]."</p>";
    }
  }
  // on retourne les données du formulaire à travers le sous-tableau $retour["corps"]
  $retour["corps"] = $donnees;
  return $retour;
}

/*
function updateUtilisateur()
Fonction de mise à jour des données d'un membre.
*/
function updateUtilisateur() {
  // On définit un titre pour la page
  $retour["titre"] = "Mise à jour de l'utilisateur";

  // Si on n'a pas d'id en GET, on redirige vers la page de listing.
  if(empty($_GET["id"])) {
    header("location:index.php?page=utilisateurs");
    exit;
  }
  $id = intval($_GET["id"]);

  // On vérifie que le membre qu'on veut mettre à jour est bien le notre si on n'est pas admin.
  if($_SESSION["utilisateur"]["role"] != 9) {
    // on appella fonction verifUserId qui va arrêter l'exécution du code si l'id en GET n'est pas le même que l'id en SESSION.
    verifUserId($id);
  }


  if($_SERVER["REQUEST_METHOD"] == "POST") {
    $donnees = verifFormulaire("update");
    var_dump($donnees);
    // si le tableau d'erreur est vide, c'est que le formulaire est validé
    if(empty($donnees["erreur"])) {
      // On utilise une structure try catch pour lancer notre requête d'insertion.
      try {
        // si on a essayé d'uploader une photo
        if(!empty($donnees["donnees"]["photo"])) {
          echo "TEST";
          // on enregistre la photo dans le dossier uploads.
          if($idPhoto = savePhoto($donnees["donnees"]["photo"]["ext"], "photo", $donnees["donnees"]["photo"]["mime"], "utilisateur")) { // si ça fonctionne
            // On réécrit les uri des photos qu'on vient d'uploader, et on les ajoute à notre tableau $donnees["donnees"] pour les transmettre à notre fonction saveUtilisateur qui va les enregistrer en base.
            $donnees["donnees"]["photo2"] = $idPhoto."-thumb".$donnees["donnees"]["photo"]["ext"];
            $donnees["donnees"]["photo"] = $idPhoto."-large".$donnees["donnees"]["photo"]["ext"];
          }
        }

        // on appelle la fonction saveUtilisateur avec comme paramètres notre tableau de données renvoyé par la fonction verifFormulaire et avec le paramètre insert

        saveUtilisateur($donnees["donnees"], "update");
        //si l'insertion s'est bien effectuée, on redirige vers la page de listing des utilisateurs

        // Si on vient de se mettre à jour soi-même, on met à jour le tableau $_SESSION.

        if($_SESSION["utilisateur"]["id"] == $id) {
          $_SESSION["utilisateur"]["nom"] = $donnees["donnees"]["nom"];
          $_SESSION["utilisateur"]["prenom"] = $donnees["donnees"]["prenom"];
          $_SESSION["utilisateur"]["login"] = $donnees["donnees"]["login"];
          $_SESSION["utilisateur"]["age"] = $donnees["donnees"]["age"];
          $_SESSION["utilisateur"]["classe"] = $donnees["donnees"]["classe"];


          header("location:index.php?page=utilisateurs&action=update&id=".$id."&mess=update1");
          exit;
        }

        header("location:index.php?page=utilisateurs&mess=update1");
        exit;
      } catch(Exception $e) {
        // s'il y a eu un problème dans l'insertion on attrape l'exception et on en renvoie une nouvelle avec le même message à notre fonction appelante (dans le contrôleur).
        throw new Exception ($e->getMessage());
      }

    } else {
      // si on a des erreurs dans notre vérification du formulaire, on transforme le sous-tableau en chaîne avec implode.
      $donnees["erreur"] = implode("<br />", $donnees["erreur"]);
      $donnees["erreur"] = "<p class='erreur'>".$donnees["erreur"]."</p>";
      $retour["corps"]["erreur"] = $donnees["erreur"];
      $retour["corps"]["donnees"] = $donnees["donnees"];
    }
  } else {
    try {
      $donnees = getDetailsUtilisateur($id);
      $retour["corps"]["donnees"] = $donnees;
    } catch(Exception $e) {
      throw new Exception ($e->getMessage());
    }
  }

  return $retour;
}

/*
function getDetailsUtilisateur()
@param $id: id de l'utilisateur recherché
Cette fonction retourne toutes les infos d'un utilisateur
*/
function getDetailsUtilisateur($id) {
  // nettoyage de l'identifiant :
  $id = intval($id);
  // instanciation de connexion PDO
  $db = connect();
  // On récupère les données de l'utilisateur dont l'identifiant est passé en paramètre.
  // Requête SQL SELECT avec utilisation des alias pour renommer les noms des colonnes afin de simplifier la manipulation des données dans la page.
  $sql = "SELECT id_utilisateur AS id, nom_utilisateur AS nom, prenom_utilisateur AS prenom, login_utilisateur AS login, age_utilisateur AS age, classe_utilisateur AS classe, email_utilisateur AS email, lat_utilisateur AS lat, lng_utilisateur AS lng, adresse_utilisateur AS adresse, ville_utilisateur AS ville, cp_utilisateur AS cp, img1_utilisateur AS photoLarge, img2_utilisateur AS photoVignette
  FROM crud_utilisateur WHERE id_utilisateur= :id";
  // 2 - Envoi de la requête avec la méthode try catch
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':id', $id, PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();
    // Je récupère l'ensemble des données retournées par la requête grâce à fetchAll
    $utilisateur = $req->fetchAll()[0];
    // j'assigne ces données à mes variables utilisées dans mon formulaire
    return $utilisateur;

  } catch (PDOException $erreur) {
    throw new Exception ($erreur->getMessage());
  }
}



/*
function getDetailsUtilisateurByLogin()
@param $login: login de l'utilisateur recherché
Cette fonction retourne toutes les infos d'un utilisateur
*/
function getDetailsUtilisateurByLogin($login) {
  // nettoyage de l'identifiant :
  $login = htmlspecialchars($login);
  // instanciation de connexion PDO
  $db = connect();
  // On récupère les données de l'utilisateur dont l'identifiant est passé en paramètre.
  // Requête SQL SELECT avec utilisation des alias pour renommer les noms des colonnes afin de simplifier la manipulation des données dans la page.
  $sql = "SELECT id_utilisateur AS id, nom_utilisateur AS nom, prenom_utilisateur AS prenom, login_utilisateur AS login, age_utilisateur AS age, classe_utilisateur AS classe, email_utilisateur AS email
  FROM crud_utilisateur WHERE login_utilisateur= :login";
  // 2 - Envoi de la requête avec la méthode try catch
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':login', $login, PDO::PARAM_STR);
    // Exécution de la requête
    $req->execute();
    // Je récupère l'ensemble des données retournées par la requête grâce à fetchAll
    $utilisateur = $req->fetchAll()[0];
    // j'assigne ces données à mes variables utilisées dans mon formulaire
    return $utilisateur;

  } catch (PDOException $erreur) {
    throw new Exception ($erreur->getMessage());
  }
}




/*
function verifFormulaire
@param string $mode : mode d'utilisation du formulaire : create ou update
Fonction vérifiant les champs du formulaire.
Retourne un tableau composé de deux sous-tableaux :
- erreur : contient les éventuelles erreurs sur les champs
- donnees : contient les données validées de chaque champ
*/
function verifFormulaire($mode = "create") {
  $retour = array(
    "erreur" => array(),
    "donnees" => array()
  );

  $retour["donnees"]["id"] = $_POST["id"];

  if(!$retour["donnees"]["nom"] = verifTexte($_POST["nom"])) {
    $retour["erreur"]["nom"] = "La longueur de votre nom doit être comprise entre 0 et 45 caractères.";
  }
  if(!$retour["donnees"]["prenom"] = verifTexte($_POST["prenom"])) {
    $retour["erreur"]["prenom"] = "La longueur de votre prénom doit être comprise entre 0 et 45 caractères.";
  }
  if(!$retour["donnees"]["email"] = verifMail($_POST["email"])) {
    $retour["erreur"]["email"] = "L'email n'est pas valide.";
  }
  if(!$retour["donnees"]["login"] = verifTexte($_POST["login"])) {
    $retour["erreur"]["login"] = "La longueur de votre login doit être comprise entre 0 et 45 caractères.";
  }
  if(!$retour["donnees"]["age"] = verifAge($_POST["age"])) {
    $retour["erreur"]["age"] = "Votre âge doit être compris entre 17 et 127 ans.";
  }
  if(!$retour["donnees"]["classe"] = verifTexte($_POST["classe"], 21)) {
    $retour["erreur"]["classe"] = "La longueur de votre classe doit être comprise entre 0 et 21 caractères.";
  }

  // mot de passe
  if(isset($_POST["pass"])) {
    if(verifPass($_POST["pass"])) {
      $retour["donnees"]["pass"] = verifPass($_POST["pass"]);
    } else {
      $retour["erreur"]["pass"] = "Le mot de passe n'est pas valide !";
    }

  }

  // Champs non obligatoires, pas de test dessus
  if(empty($_POST["lat"])) {
    $retour["donnees"]["lat"] = null;
  } else {
    $retour["donnees"]["lat"] = htmlspecialchars($_POST["lat"]);
  }

  if(empty($_POST["lng"])) {
    $retour["donnees"]["lng"] = null;
  } else {
    $retour["donnees"]["lng"] = htmlspecialchars($_POST["lng"]);
  }

  $retour["donnees"]["adresse"] = htmlspecialchars($_POST["adresse"]);
  $retour["donnees"]["cp"] = htmlspecialchars($_POST["cp"]);
  $retour["donnees"]["ville"] = htmlspecialchars($_POST["ville"]);

  // GESTION DE LA PHOTO DE PROFIL
  // On utilise la fonction verifPhoto pour analyser les éventuelles erreurs d'upload de fichier.
  if($mode == "update") {
    $photo = verifPhoto("photo", false); // vérification de la photo en mode non obligatoire grâce au paramètre false
  } else {
    $photo = verifPhoto("photo");
  }

  if($photo["valid"] == false) { // on a un probleme
    $retour["erreur"]["photo"] = $photo["message"]; // on récupère le message d'erreur pour l'afficher.
  } else { // pas de problème, on récupère juste l'extension.
    // en cas d'update avec photo inexistante, on peut avoir $photo["valid"] à true, mais avec un message.
    // On se sert de ce comportement pour ne pas transmettre les données de la photo si on est dans ce cas.
    if(empty($photo["message"])) {
      $retour["donnees"]["photo"]["ext"] = $photo["ext"];
      $retour["donnees"]["photo"]["mime"] = $photo["mime"];
    }
  }


  return $retour;
}

/*
Cette fonction permet de supprimer un utilisateur
*/
function deleteUtilisateur() {
  // instanciation d'une connexion PDO
  $db = connect();

  // Si on n'a pas d'id en GET, on redirige vers la page de listing.
  if(empty($_GET["id"])) {
    header("location:index.php?page=utilisateurs");
    exit;
  }
  $id = $_GET["id"];

  // On vérifie que l'utilisateur qu'on veut supprimer est bien le notre si on n'est pas admin.
  if($_SESSION["utilisateur"]["role"] != 9) {
    verifUserId($id);
  }

  // Suppression des éventuels fichiers de photos de profil de l'utilisateur avant la suppression de l'utilisateur en base.
  try {
    suppressionPhoto("private");
  } catch (Exception $e) {
    throw new Exception($e->getMessage());
  }

  // Requête de suppression d'un utilisateur ciblé par son id
  // 1 - écriture de la requête SQL DELETE.
  $sql = "DELETE FROM crud_utilisateur WHERE id_utilisateur= :id";
  // 2 - Envoi de la requête avec la méthode try catch
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':id', $_GET["id"], PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();

    // Si on est pas admin, on atterit sur l'accueil
    if($id == $_SESSION["utilisateur"]["id"]) {
      header("location:index.php?mess=delete2");
      exit;
    } else {
      header("location:index.php?page=utilisateurs&mess=delete1");    exit;
    }

  } catch (PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }

}

function afficheRole($role) {
  $tabRole = array(
    9 => "Super Admin",
    0 => "utilisateur"
  );
  return $tabRole[$role];
}

function verifUserId($id) {
  if($id != $_SESSION["utilisateur"]["id"]) {
    header("location:index.php?page=utilisateurs");
    exit;
  }
}


function recupPass() {
  $page["titre"] = "Récupérer votre mot de passe !";
  if($_SERVER["REQUEST_METHOD"] == "POST") {
    // on commence par tester si le login est conforme à nos règles à l'échelle du site. Si ce n'est pas le cas, on ne va pas plus loin.
    if(($login = verifTexte($_POST["login"])) && !empty($_POST["login"])) {
      // on récupère les données de l'utilisateur avec une requête
      if($utilisateur = getUtilisateurByLogin($login)) {
        // on récupère le mail destinataire.
        $email = $utilisateur[0]["email_utilisateur"];
        $nom = ucfirst($utilisateur[0]["nom_utilisateur"]);
        $prenom = ucfirst($utilisateur[0]["prenom_utilisateur"]);
        // génération du nouveau mot de passe + insertion en base
        $pass = generationPass($login, $utilisateur[0]["id_utilisateur"]);

        // création du mail : on va utiliser la classe PHPMailer qui va simplifier la construction et l'envoi du mail.
        // Pour des mails complexes au format HTML, c'est recommandé de passer par une classe de ce type afin d'augmenter la délivrabilité du mail.

        // on écrit notre contenu
        $contenuMail = "
        <h1>Réinitialisation de votre mot de passe</h1>
        <p>Bonjour ".$prenom." ".$nom.",<br />
        Vous avez demandé la réinitialisation de votre mot de passe pour le site crud.com. Voici votre nouveau mot de passe : ".$pass."<br />
        Nous vous conseillons par mesure de sécurité de le modifier lors de votre prochaine connexion.<br />
        A bientôt !
        </p>";

        // On va charger le fichier de vue contenant notre template d'email, qui va intégrer automatiquement la variable $contenuMail
        require("vues/vueMail.php");

        // Pour debug : echo $template;

        // On utilise PHPMailer pour construire le mail
        // instanciation d'un nouvel objet de la classe PHPMailer
        $mail = new PHPMailer;
        // définition des expéditeurs et destinataires
        $mail->setFrom('webmaster@crud.com', 'Webmaster du site Crud.com');
        $mail->addAddress($email, $prenom." ".$nom);     // Add a recipient
        // définition du format du mail (HTML)
        $mail->isHTML(true);
        // définition du contenu
        $mail->Subject = 'Réinitialisation de votre mot de passe';
        $mail->Body    = $template;
        $mail->AltBody = "Rénitialisation de votre mot de passe sur crud.com : Votre nouveau mot de passe est le suivant : ".$pass."\r\n
        Nous vous conseillons de le modifier lors de votre prochaine connexion.";

        // Envoi du mail
        if(!$mail->send()) {
          // si l'envoi a échoué, on retourne un message d'erreur
          $page["erreur"] = "L'email n'a pas pu vous être envoyé : ".$mail->ErrorInfo;
        } else {
          // sinon on confirme la réinitialisation du mot de passe.
            $page["erreur"] = "Si votre login est enregistré dans notre site, vous allez recevoir un email vous permettant de retrouver votre mot de passe.";
        }
      } else {
        // on a pas trouvé d'utilisateur pour ce login, mais on ne le dit pas à l'utilisateur pour éviter de donner des informations à des personnes malveillantes.
        $page["erreur"] = "Si votre login est enregistré dans notre site, vous allez recevoir un email vous permettant de retrouver votre mot de passe.";
      }
    } else {
      $page["erreur"] = "Merci de rentrer votre login dans le champ pour réinitialiser votre mot de passe !";
    }
  }
  return $page;
}

/*
Fonction générant un nouveau mot de passe pseudo aléatoire, et l'enregistrant en base de donnée pour l'id utilisateur transmis en paramètre.
*/
function generationPass($login, $id) {
  $db = connect();
  $pass = substr(md5($login.$id.uniqid()), 5, 12);
  $passHash = password_hash($pass, PASSWORD_DEFAULT);
  $sql = "UPDATE crud_utilisateur SET pass_utilisateur = :passHash WHERE id_utilisateur = :id";

  // On utilise un try catch pour lancer la requête PDO.
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':passHash', $passHash, PDO::PARAM_STR);
    $req->bindParam(':id', $id);
    // Exécution de la requête
    $req->execute();

    return $pass;

  } catch (PDOException $erreur) {
    // si la requête PDO échoue, on attrape l'exception de type PDOException et on envoie une nouvelle exception en reprenant le même message.
    throw new Exception ($erreur->getMessage());
  }
}




/*
function suppressionPhoto()
Fonction de suppression de la photo de profil, en base et en fichiers
@param string $mode : mode d'utilisation de la fonction : public ou private
*/
function suppressionPhoto($mode="public") {
 // 0 - Contrôle des accès et des rôles.
 // Si on n'a pas d'id en GET, on redirige vers la page de listing.
 if(empty($_GET["id"])) {
   header("location:index.php?page=utilisateurs");
   exit;
 }
 $id = intval($_GET["id"]);

 // On vérifie que le membre qu'on veut mettre à jour est bien le notre si on n'est pas admin.
 if($_SESSION["utilisateur"]["role"] != 9) {
   // on appella fonction verifUserId qui va arrêter l'exécution du code si l'id en GET n'est pas le même que l'id en SESSION.
   verifUserId($id);
 }

 // 1 - récupération des URLS des photos en base
$utilisateur = getDetailsUtilisateur($id);
$photoLarge = $utilisateur["photoLarge"];
$photoVignette = $utilisateur["photoVignette"];

if(!empty($photoLarge) && !empty($photoVignette)) {
  // 2 - requête UPDATE pour supprimer les photos en base
  $db = connect();
  $sql = "UPDATE crud_utilisateur SET img1_utilisateur = null, img2_utilisateur = null WHERE id_utilisateur = ".$id.";";

  try {
   $requete = $db->query($sql);
   // 3 - suppression des fichiers de photo non utilisés.
   if(unlink(BASE_URL."/uploads/".$photoLarge)) {
     if(unlink(BASE_URL."/uploads/".$photoVignette)) {
         // Fonction réussie, on redirige vers la fiche utilisateur
         if($mode == "public") {
           header("location:index.php?page=utilisateurs&action=update&id=".$id."&mess=suppImage1");
           exit;
         } else {
           return true;
         }
       } else {
         throw new Exception("L'image n'a pas pu être supprimée (problème de destruction du fichier.)");
       }
     } else {
     throw new Exception("L'image n'a pas pu être supprimée (problème de destruction du fichier.)");
     }
   } catch(PDOException $erreur) {
       throw new Exception ($erreur->getMessage());
   }
} else {
  // Fonction réussie, on redirige vers la fiche utilisateur
  if($mode == "public") {
    header("location:index.php?page=utilisateurs&action=update&id=".$id."&mess=noImage");
    exit;
  } else {
    return true;
  }
}
}




/*
Fonction permettant de générer un export CSV de la liste des utilisateurs
*/
function exportUtilisateurs() {
  // on instancie à vide une variable $retour
  $retour = "";
  // On récupère la liste de nos utilisateurs avec notre fonction listeUtilisateurs du modèle
  $liste = listeUtilisateurs();
  // on parcourt les résultats de la liste
  while ($utilisateur = $liste["corps"]->fetch()) {
    // A chaque ligne retournée, on écrit une nouvelle ligne dans la variable $retour
    $retour .= $utilisateur["id_utilisateur"].";".$utilisateur["nom_utilisateur"].";".$utilisateur["prenom_utilisateur"]."\n";
  }
  // Pour assurer la compatibilité avec Excel qui ne supporte pas l'UTF8, on décode le contenu de $retour
  $retour = utf8_decode($retour);

  // Enregistrement du fichier
  // On définit un répertoire d'enregistrement du fichier
  $repertoire = BASE_URL."/exports";

  // On regarde si on a déjà des fichiers .csv dans le répertoire.
  $fichiers = glob($repertoire."/*.csv");
  foreach ($fichiers as $fichier) {
    // Si c'est le cas on peut les supprimer avec unlink
    // unlink($fichier);
  }

  // on écrit le nom du fichier avec uniqid pour avoir un nom unique
  $nomFichier = "export-crud-".uniqid().".csv";

  // On écrit le fichier sur le serveur
  file_put_contents(BASE_URL."/exports/".$nomFichier, $retour);

  // On va générer une sortie HTTP contenant directement le fichier .
  // Pour cela on va utiliser les fonctions header
  // Envoi des en-têtes HTTP
  header('Content-Disposition: attachment; filename="'.$nomFichier.'"');
  header("Content-Type: application/force-download");
  // Envoi du corps HTTP en lisant le fichier
  readfile(BASE_URL.'/exports/'.$nomFichier);
  // Fermeture du fichier
  header("Connection: close");
  // unlink(BASE_URL."/exports/".$nomFichier);
  exit;
}
