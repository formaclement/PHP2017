<?php
// Modèle des illustrations.
// Le modèle est le composant MVC chargé de s'occuper de la logique métier de notre application.
// C'est dans le modèle qu'on va trouver les fonctions de récupération de données dans notre base.
// C'est aussi ici qu'on va traiter ces données avant enregistrement ou affichage par transmission vers la vue.
// On charge le modele de connexion à la BDD avec require_once pour éviter les erreurs dues à la redéclaration de fonctions.
require_once("modeleBdd.php");
require_once("modeleSession.php");
require_once("helpersForm.php");
require_once("modelePhoto.php");

/*
Function listeIllustrations()
Fonction listant les illustrations présentes sur le site
*/
function listeIllustrations() {

  $retour["titre"] = "Liste des illustrations";

  $db = connect();
  // envoyer une requête SQL SELECT sur la table des illustrations
  $sql = "SELECT id_illus, url_illus, texte_illus FROM crud_illus ORDER BY id_illus DESC";
  try {
    // Renvoyer le resultat sous la forme d'un tableau pré-formaté.
      $retour["illus"] = $db->query($sql);
    return $retour;
  } catch(PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }
}

/*
function getIllustrationById($id)
Fonction permettant de récupérer l'url de l'illustration à partir de son id en base
@param string $id : l'id en base de la ligne de l'illustration
*/
function getUrlIllustrationById($id) {
  try {
    $db = connect();
    // envoyer une requête SQL SELECT sur la table des illustrations
    $sql = "SELECT url_illus FROM crud_illus WHERE id_illus = :idPhoto";
    // on prépare la requête
    $req = $db->prepare($sql);
    // on lie le texte alternatif
    $req->bindParam(':idPhoto', $id, PDO::PARAM_INT);
    $req->execute();
    return $req->fetchAll()[0];
  } catch(PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }
}



/*
Function createIllustration()
Fonction créant une nouvelle entrée en base et un nouvel enregistrement de fichier.
*/
function createIllustration() {
  if($_SERVER["REQUEST_METHOD"] == "POST") {
    // 1 - traitement du formulaire
    $donnees = verifFormulaireIllustrations();
    if(empty($donnees["erreur"])) {
      // on n'a pas d'erreur, on peut enregistrer les données
      // on utilise la fonction savePhoto qui en cas de succès, nous retourne la fin de l'url de la photo.
      if($urlPhoto = savePhoto($donnees["donnees"]["photo"]["ext"], "photo", $donnees["donnees"]["photo"]["mime"], "illustration")) {
        // à ce stade, la photo est enregistrée dans les fichiers du serveur. On peut l'enregistrer en base.
        // instanciation de connexion PDO
        $db = connect();
        // Je réécris la fin de l'url de la photo avec son extension.
        $urlPhoto = $urlPhoto.$donnees["donnees"]["photo"]["ext"];
        try {
          // Enregistrement SQL de la photo.
          $sql = "INSERT INTO crud_illus VALUES (null, :urlPhoto, :texteAlt);";
          // on prépare la requête
          $req = $db->prepare($sql);
          // on lie l'url de la photo et le texte alternatif
          $req->bindParam(':urlPhoto', $urlPhoto, PDO::PARAM_STR);
          $req->bindParam(':texteAlt', $donnees["donnees"]["texte"], PDO::PARAM_STR);
          // on éxécute la requête
          $req->execute();
          header("location:index.php?page=illustrations&mess=newIllus1");
          exit;

        } catch (PDOException $erreur) {
          throw new Exception($erreur->getMessage());
        }
      } else {
        // Problème d'écriture du fichier image
        throw new Exception("L'image d'illustration n'a pas pu être enregistrée : Problèmre d'écriture du fichier image.");
      }
    } else {
      $retourErreur = "";
      foreach ($donnees["erreur"] as $erreur) {
        $retourErreur .= $erreur."<br />";
      }
      header("location:index.php?page=illustrations&mess=newIllus0");
      exit;
    }
  } else {
    header("location:index.php?page=illustrations");
    exit;
  }

}

/*
function destructionFichiersIllus()
Fonction détruisant les fichiers des illustrations en base de données
@param string $urlFin : url de fin des fichiers tel qu'enregistré dans la base
*/
function destructionFichiersIllus($urlFin) {
  $thumb = BASE_URL.'/uploads/illustrations/thumb-'.$urlFin;
  $medium = BASE_URL.'/uploads/illustrations/medium-'.$urlFin;
  $large = BASE_URL.'/uploads/illustrations/large-'.$urlFin;
  if(unlink($thumb) && unlink($medium) && unlink($large)) {
    return true;
  } else {
    return false;
  }
}

/*
function deleteIllustration()
Fonction permettant de supprimer une illustration
*/
function deleteIllustration() {
  // Si on n'est pas admin, on est redirigé
  if($_SESSION["utilisateur"]["role"] != 9) {
    header("location:index.php?page=illustrations");
    exit;
  }
  if(!empty($_GET["id"])) {
    $idPhoto = htmlspecialchars($_GET["id"]);

    // on récupère l'url de fin de l'illustration.
    try {
      $illus = getUrlIllustrationById($idPhoto);
      $urlIllus = $illus["url_illus"];
      // on supprime les fichiers avec notre fonction destructionFichiersIllus()
        destructionFichiersIllus($urlIllus);
        // Suppression de la photo
        try {
          $db = connect();
          // Enregistrement SQL de la photo.
          $sql = "DELETE FROM crud_illus WHERE id_illus = :idPhoto";
          // on prépare la requête
          $req = $db->prepare($sql);
          // on lie le texte alternatif
          $req->bindParam(':idPhoto', $idPhoto, PDO::PARAM_INT);
          // on éxécute la requête
          $req->execute();

          header("location:index.php?page=illustrations&mess=deleteIllus1");
          exit;

        } catch (PDOException $erreur) {
          throw new Exception($erreur->getMessage());
        }
    } catch(Exception $erreur) {
      throw new Exception($erreur->getMessage());
    }

  } else {
    throw new Exception("id non précisé !");
  }
}


/*
function updateIllustration()
Fonction permettant de mettre à jour l'illustration et son texte alternatif
*/
function updateIllustration() {
  // Si on n'est pas admin, on est redirigé
  if($_SESSION["utilisateur"]["role"] != 9) {
    header("location:index.php?page=illustrations");
    exit;
  }
  if(!empty($_GET["id"])) {
    $idPhoto = htmlspecialchars($_GET["id"]);

      // Si une photo a été transmise par l'utilisateur
    if($_FILES["photo"]["error"] != 4) {
      $donnees = verifFormulaireIllustrations();
      if(empty($donnees["erreur"])) { // je n'ai pas d'erreur, j'enregistre
        // j'enregistre les fichiers et récupère la fin de leur url
        if($urlPhoto = savePhoto($donnees["donnees"]["photo"]["ext"], "photo", $donnees["donnees"]["photo"]["mime"], "illustration")) {

          $urlPhoto = $urlPhoto.$donnees["donnees"]["photo"]["ext"];

          // Je récupère l'url de la photo existante pour pouvoir supprimer les fichiers une fois que mon update sera terminé.
          $ancienneUrl = getUrlIllustrationById($idPhoto)["url_illus"];

          // j'enregistre en base mon url et mon texte
          try {
            $db = connect();
            // envoyer une requête SQL SELECT sur la table des illustrations
            $sql = "UPDATE crud_illus SET texte_illus = :texte, url_illus = :urlPhoto WHERE id_illus = :idPhoto ;";
            // on prépare la requête
            $req = $db->prepare($sql);
            // on lie le texte alternatif
            $req->bindParam(':texte', $donnees["donnees"]["texte"], PDO::PARAM_STR);
            $req->bindParam(':urlPhoto', $urlPhoto, PDO::PARAM_STR);
            $req->bindParam(':idPhoto', $idPhoto, PDO::PARAM_INT);
            $req->execute();

            // Pour finir, je supprime les photos précédentes

            if(destructionFichiersIllus($ancienneUrl)) {
              header("location:index.php?page=illustrations&mess=modifIllus1");
              exit;
            }

          } catch(PDOException $erreur) {
            throw new Exception($erreur->getMessage());
          }
        }

      } else {
        header("location:index.php?page=illustrations&mess=modifIllus0#".$idPhoto);
        exit;
      }
    } else {
      // si on a juste le texte
      if($texte = verifTexteMin($_POST["texte"], 10)) {
        $db = connect();
        // envoyer une requête SQL SELECT sur la table des illustrations
        $sql = "UPDATE crud_illus SET texte_illus = :texte WHERE id_illus = :idPhoto ;";
        // on prépare la requête
        $req = $db->prepare($sql);
        // on lie le texte alternatif
        $req->bindParam(':texte', $texte, PDO::PARAM_STR);
        $req->bindParam(':idPhoto', $idPhoto, PDO::PARAM_INT);
        $req->execute();

        // Redirection
        header("location:index.php?page=illustrations&mess=modifIllus1");
        exit;

      } else {
        header("location:index.php?page=illustrations&mess=texteCourt#".$idPhoto);
        exit;
      }

    }


  } else {
    throw new Exception("id non précisé !");
  }
}

/*
Function verifFormulaireIllustrations()
Fonction permettant de vérifier les champs du formulaire d'ajout ou de modif des illustrations
*/
function verifFormulaireIllustrations() {
  $retour = array(
    "erreur" => array(),
    "donnees" => array()
  );

  // Vérification du texte alt
  if(!$retour["donnees"]["texte"] = verifTexteMin($_POST["texte"], 10)) {
    $retour["erreur"]["texte"] = "La longueur du texte alternatif doit être de minimum 10 caractères.";
  }

  // Vérification de la photo
  $photo = verifPhoto("photo");
  if($photo["valid"] == false) { // on a un probleme
    $retour["erreur"]["photo"] = $photo["message"]; // on récupère le message d'erreur pour l'afficher.
  } else { // pas de problème, on récupère juste l'extension.
    // en cas d'update avec photo inexistante, on peut avoir $photo["valid"] à true, mais avec un message.
    // On se sert de ce comportement pour ne pas transmettre les données de la photo si on est dans ce cas.
    if(empty($photo["message"])) {
      $retour["donnees"]["photo"]["ext"] = $photo["ext"];
      $retour["donnees"]["photo"]["mime"] = $photo["mime"];
    }
  }

  return $retour;
}











?>
