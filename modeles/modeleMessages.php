<?php
/*
Ce modèle sert à gérer les messages automatiques avec l'utilisateur.
*/

/*
function getAutomatiques() : permet de gérer les messages retour à l'utilisateur
@param string $code : paramètre optionnel

La fonction s'utilise en deux modes :
Sans paramètre elle analyse $_GET["mess"] et retourne le message correspondant
Avec paramètre elle retourne le message correspondant au paramètre.

*/
function getAutomatiques($code = "") {
  $tabMessages = array(
    "insert1" => "La création du contenu a bien été effectuée.",
    "update1" => "La mise à jour du contenu a bien été effectuée.",
    "delete1" => "Le contenu a bien été supprimé.",
    "delete2" => "Vous avez bien été supprimé du site, au revoir !",
    "prive" => "Vous devez être connecté pour accéder à cette page !",
    "sess0" => "Vous êtes bien déconnecté du site.",
    "PDO23000" => "Cette donnée existe déjà !",
    "suppImage1" => "l'image de profil a bien été supprimée !",
    "noImage" => "Pas de photo à supprimer pour cet utilisateur",
    "newIllus1" => "L'image a bien été enregistrée !",
    "newIllus0" => "L'image n'a pas pu être enregistrée !",
    "deleteIllus1" => "L'image a bien été supprimée !",
    "texteCourt" => "Le texte est trop court !",
    "modifIllus0" => "L'illustration n'a pas pu être modifiée !",
    "modifIllus1" => "L'illustration a bien été modifiée !"

  );

  $message = "";

  if(!empty($code)) {
    $message = $tabMessages[$code];
  } else {
    if(!empty($_GET["mess"]) && array_key_exists($_GET["mess"], $tabMessages)) {
      $message = "<p class='message'>".$tabMessages[$_GET["mess"]]."</p>";
    }
  }

  return $message;

}
