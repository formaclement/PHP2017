<?php
/*
Modèle servant à gérer les manipulations d'enregistrement et de suppression de photos
*/

/*
function savePhoto
Permet d'enregistrer une photo et de la redimensionner avec la classe Claviska\SimpleImage
@param string $extension : l'extension finale de la photo.
@param string $nomInput : nom de l'input gérant la photo
@param string $mode : mode d'utilisation de la fonction (utilisateur ou illustration)
*/
function savePhoto($extension, $nomInput, $typeMime, $mode) {
  // on commence par créer un identifiant unique grâce à uniqid
  $id = uniqid();
  // On récupère le nom du fichier temporaire envoyé
  $tmpFile = $_FILES[$nomInput]["tmp_name"];
  // On va instancier un nouvel objet de la classe SimpleImage qui va nous aider à manipuler notre image (rotation, redimensionnement, enregistrement)
  $image = new \claviska\SimpleImage();
  // on load le fichier temporaire dans l'objet.
  $image->fromFile($tmpFile);
  // j'utilise la méthode getExif pour récupérer les infos exif de l'image.
  // si j'ai une donnée concernant l'orientation, j'utilise la méthode autoOrient.
  if(isset($image->getExif()["Orientation"])) {
    $image->autoOrient();
  }
  // Enregistrement de l'image
  // Redimensionnement d'une image grand format + enregistrement
  if($mode == "utilisateur") {
    $dossier = BASE_URL."/uploads";
    try{
      $image->bestFit(800, 600)->toFile($dossier."/".$id."-large".$extension, $typeMime, 80);

      try{
        $image->thumbnail(150, 150)->toFile($dossier."/".$id."-thumb".$extension, $typeMime, 80);
        return $id;
      } catch(Exception $e) {
        return(false);
      }
    } catch(Exception $e) {
      return(false);
    }
  } else if ($mode == "illustration") {
    $dossier = BASE_URL."/uploads/illustrations";
    try{
      $image->bestFit(1200, 800)->toFile($dossier."/large-".$id.$extension, $typeMime, 80);

      try{
        $image->thumbnail(150, 150)->toFile($dossier."/thumb-".$id.$extension, $typeMime, 80);

        try {
          $image->thumbnail(350, 350)->toFile($dossier."/medium-".$id.$extension, $typeMime, 80);
          return $id;
        } catch(Exception $e) {
          return(false);
        }
      } catch(Exception $e) {
        return(false);
      }
    } catch(Exception $e) {
      return(false);
    }

  } else {
    throw new Exception("Paramètre \$mode inconnu dans la fonction savePhoto");
  }

}

/*
function getUrlIllustration()
Fonction retournant l'URL d'affichage d'une illustration
@param string $id : l'identifiant en base de l'illustration
@param string $taille : la taille de l'image (thumb, medium, large)
*/
function getUrlIllustration($id, $taille="medium") {
  return URL."/uploads/illustrations/".$taille."-".$id;
}

/*
function theUrlIllustration()
Fonction ecrivant l'URL d'affichage d'une illustration
@param string $id : l'identifiant en base de l'illustration
@param string $taille : la taille de l'image (thumb, medium, large)
*/
function theUrlIllustration($id, $taille="medium") {
  echo getUrlIllustration($id, $taille);
}


?>
