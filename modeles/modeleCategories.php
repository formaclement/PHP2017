<?php
// Modèle des catégories.
// Le modèle est le composant MVC chargé de s'occuper de la logique métier de notre application.
// C'est dans le modèle qu'on va trouver les fonctions de récupération de données dans notre base.
// C'est aussi ici qu'on va traiter ces données avant enregistrement ou affichage par transmission vers la vue.
// On charge le modele de connexion à la BDD avec require_once pour éviter les erreurs dues à la redéclaration de fonctions.
require_once("modeleBdd.php");
require_once("modeleSession.php");
require_once("helpersForm.php");
require_once("modeleMessages.php");


/*
function getCategories()
Fonction retournant la liste des categories
*/
function getCategories() {
  $db = connect();
  // Code de récupération de notre liste d'actualités
  // 1 - écriture de la requête SQL SELECT : On fait une jointure entre crud_utilisateur et crud_actu.
  // Premier exemple de jointure avec la clause Where
  $sql = "SELECT id_cat, nom_cat FROM crud_cat";

  // 2 - Envoi de la requête avec la méthode try catch
  try {
    $retour["donnees"] = $db->query($sql);
    $retour["statut"] = "ok";
  } catch (PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }
  return $retour;
}


/*
Fonction createCategorie()
Cette fonction permet de gérer les infos relatives à la page de création d'une nouvelle categorie
*/
function createCategorie() {
  if($_SESSION["utilisateur"]["role"] != 9) {
    header("location:index.php?page=utilisateur");
    exit;
  }
  // On définit le titre de la page
  $retour["titre"] = "Création d'une nouvelle catégorie";

  // Définition des variables des données du formulaire (par défaut, tout est vide)
  $donnees["donnees"]["id_cat"] = "NULL";
  $donnees["donnees"]["nom_cat"] = "";

  // Si on vient de soumettre le formulaire, on passe dans la condition suivante
  if($_SERVER["REQUEST_METHOD"] == "POST") {
    // On appelle la fonction verifFormulaireCatégories, qui va analyser les données transmises en POST et qui retourne un tableau composé de deux sous-éléments :
    // - les erreurs sous la forme d'un sous tableau
    // - Les données validées sous la forme d'un sous tableau
    $donnees = verifFormulaireCategories();

    // si le tableau d'erreur est vide, c'est que le formulaire est validé
    if(empty($donnees["erreur"])) {

      // On utilise une structure try catch pour lancer notre requête d'insertion.
      try {

        // on appelle la fonction saveCategorie avec comme paramètres notre tableau de données renvoyé par la fonction verifFormulaire et avec le paramètre insert
        // la fonction saveCategorie retourne l'identifiant de la nouvelle catégorie enregistrée.
        $newId = saveCategorie($donnees["donnees"], "insert");
        // On ajoute l'identifiant aux données.
        $donnees["donnees"]["id_cat"] = $newId;
        $retour["corps"] = $donnees;
        //si l'insertion s'est bien passée, on retourne un message de validation.
        $retour["statut"] = "reussite";
        return $retour;
      } catch(Exception $e) {
        // on récupère les exceptions si besoin et on les ajoute dans le retour json qui sera utilisé par AJAX.
        $donnees["erreur"] = $e->getMessage();
        $retour["corps"] = $donnees;
        $retour["statut"] = "probleme";
        return $retour;
      }

    } else {
      // si on a des erreurs dans notre vérification du formulaire, on transforme le sous-tableau en chaîne avec implode.
      $donnees["erreur"] = implode("<br />", $donnees["erreur"]);
      $donnees["erreur"] = "<p class='erreur'>".$donnees["erreur"]."</p>";
    }
  }
  // on retourne les données du formulaire à travers le sous-tableau $retour["corps"]
  $retour["corps"] = $donnees;
  $retour["statut"] = "probleme";
  return $retour;
}



/*
function verifFormulaireCategories
Fonction vérifiant les champs du formulaire.
Retourne un tableau composé de deux sous-tableaux :
- erreur : contient les éventuelles erreurs sur les champs
- donnees : contient les données validées de chaque champ
*/
function verifFormulaireCategories() {
  $retour = array(
    "erreur" => array(),
    "donnees" => array()
  );

  $retour["donnees"]["id_cat"] = $_POST["id_cat"];

  if(!$retour["donnees"]["nom_cat"] = verifTexteMin($_POST["nom_cat"], 2)) {
    $retour["erreur"]["nom_cat"] = "Le nom de la catégorie est trop court !";
  }
  return $retour;
}

/*
Fonction saveCategorie
@param array $donnees : tableau des données relatives à la categorie
@param string $mode : le mode d'utilisation de la fonction : en insert ou en update
Cette fonction a pour rôle d'enregistrer un utilisateur dans la BDD, que ce soit en insert ou en update
*/
function saveCategorie($donnees, $mode) {
  // on commence par instancier une nouvelle connexion PDO
  $db = connect();

  $role = "0";

  // On teste si le tableau $donnees n'est pas conforme à nos attentes (s'il est vide, ou si ce n'est pas un tableau), si c'est le cas, on envoie une nouvelle exception.
  if(empty($donnees) || !is_array($donnees)) {
    throw new Exception("Les données à enregistrer ne sont pas valides !");
  }
  // On teste le deuxième paramètre de notre fonction ($mode), qui doit avoir deux valeurs possibles : "insert" ou "update".
  // En fonction de ces valeurs, on va écrire une requête SQL différente.
  if($mode == "insert") {
    // requete SQL d'insertion
    $sql = "INSERT INTO crud_cat VALUES (:id, :nom)";
  } else if ($mode == "update") {
    // requete SQL d'update
    $sql = "UPDATE crud_cat
    SET nom_cat = :nom
    WHERE id_cat = :id";
  } else {
    // si la valeur de $mode n'est pas conforme, on envoie une nouvelle exception.
    throw new Exception ("le mode d'utilisation de la fonction n'est pas reconnu !");
  }

  // On utilise un try catch pour lancer la requête PDO.
  try {
    if($donnees["id_cat"] == "NULL") {
      $donnees["id_cat"] = null;
    }
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':nom', $donnees["nom_cat"], PDO::PARAM_STR);
    $req->bindParam(':id', $donnees["id_cat"]);
    // Exécution de la requête
    $req->execute();
    // on récpère l'identifiant de la catégorie nouvellement créée et on la retourne.
    return $db->lastInsertId();
  } catch (PDOException $erreur) {
    // si la requête PDO échoue, on attrape l'exception de type PDOException et on envoie une nouvelle exception en reprenant le même message.

    // on utilise le modèle des messages pour retourner un message "humain"
    throw new Exception (getAutomatiques("PDO".$erreur->getCode()), $erreur->getCode());
  }
}


/*
function updateCategorie()
Fonction de mise à jour des données d'une catégorie
*/
function updateCategorie() {
  // On définit un titre pour la page
  $retour["titre"] = "Mise à jour de la catégorie";

  // Si on n'a pas d'id en GET, on redirige vers la page de listing.
  if(empty($_GET["id"])) {
    header("location:index.php?page=actualites");
    exit;
  }
  $id = intval($_GET["id"]);

  // On vérifie que le membre qu'on veut mettre à jour est bien le notre si on n'est pas admin.
  if($_SESSION["utilisateur"]["role"] != 9) {
    // on appella fonction verifUserId qui va arrêter l'exécution du code si l'id en GET n'est pas le même que l'id en SESSION.
    // verifUserId($id);
    /* CODE A INSERER POUR VERIFIER LA PROPRIETE DE L'ACTU */
  }


  if($_SERVER["REQUEST_METHOD"] == "POST") {
    $donnees = verifFormulaireCategories  ();
    // si le tableau d'erreur est vide, c'est que le formulaire est validé
    if(empty($donnees["erreur"])) {
      // On utilise une structure try catch pour lancer notre requête d'insertion.
      try {
        // on appelle la fonction saveUtilisateur avec comme paramètres notre tableau de données renvoyé par la fonction verifFormulaire et avec le paramètre insert
        saveCategorie($donnees["donnees"], "update");
        //si l'insertion s'est bien effectuée,
        $retour["corps"] = $donnees;
        //si l'insertion s'est bien passée, on retourne un message de validation.
        $retour["statut"] = "reussite";
        return $retour;

      } catch(Exception $e) {
        // s'il y a eu un problème dans l'insertion on attrape l'exception et on en renvoie une nouvelle avec le même message à notre fonction appelante (dans le contrôleur).
        // on récupère les exceptions si besoin et on les ajoute dans le retour json qui sera utilisé par AJAX.
        $donnees["erreur"] = $e->getMessage();
        $retour["corps"] = $donnees;
        $retour["statut"] = "probleme";
        return $retour;
      }

    } else {
      // si on a des erreurs dans notre vérification du formulaire, on transforme le sous-tableau en chaîne avec implode.
      $donnees["erreur"] = implode("<br />", $donnees["erreur"]);
      $donnees["erreur"] = "<p class='erreur'>".$donnees["erreur"]."</p>";
      $retour["corps"]["erreur"] = $donnees["erreur"];
      $retour["corps"]["donnees"] = $donnees["donnees"];
    }
  } else {
    try {
      $donnees = getDetailsCategorie($id);
      $retour["corps"]["donnees"] = $donnees;
    } catch(Exception $e) {
      throw new Exception ($e->getMessage());
    }
  }

  return $retour;
}

/*
function getDetailsCategorie()
@param $id: id de la categorie recherchée
Cette fonction retourne toutes les infos d'une catégorie
*/
function getDetailsCategorie($id) {
  // nettoyage de l'identifiant :
  $id = intval($id);
  // instanciation de connexion PDO
  $db = connect();
  // On récupère les données de l'actu dont l'identifiant est passé en paramètre.
  // Requête SQL SELECT avec utilisation des alias pour renommer les noms des colonnes afin de simplifier la manipulation des données dans la page.
  $sql = "SELECT id_cat, nom_cat FROM crud_cat WHERE id_cat = :id";
  // 2 - Envoi de la requête avec la méthode try catch
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':id', $id, PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();
    // Je récupère l'ensemble des données retournées par la requête grâce à fetchAll
    $utilisateur = $req->fetchAll()[0];
    // j'assigne ces données à mes variables utilisées dans mon formulaire
    return $utilisateur;

  } catch (PDOException $erreur) {
    throw new Exception ($erreur->getMessage());
  }
}



/*
deleteCategorie
Cette fonction permet de supprimer une catégorie
*/
function deleteCategorie() {
  // instanciation d'une connexion PDO
  $db = connect();

  // Si on n'a pas d'id en GET, on redirige vers la page de listing.
  if(empty($_GET["id"])) {
    header("location:index.php?page=actualites");
    exit;
  }
  $id = $_GET["id"];

  // On vérifie que l'utilisateur qu'on veut supprimer est bien le notre si on n'est pas admin.
  if($_SESSION["utilisateur"]["role"] != 9) {
    verifUserId($id);
  }

  // Requête de suppression d'une actualité ciblée par son id
  // 1 - écriture de la requête SQL DELETE.
  $sql = "DELETE FROM crud_cat WHERE id_cat= :id";
  // 2 - Envoi de la requête avec la méthode try catch
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':id', $_GET["id"], PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();

    $retour["statut"] = "reussite";

  } catch (PDOException $erreur) {
    // throw new Exception($erreur->getMessage());
    $retour["statut"] = "probleme";
  }

  return $retour;
}

?>
