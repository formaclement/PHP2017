<?php
// Modèle des actualités.
// Le modèle est le composant MVC chargé de s'occuper de la logique métier de notre application.
// C'est dans le modèle qu'on va trouver les fonctions de récupération de données dans notre base.
// C'est aussi ici qu'on va traiter ces données avant enregistrement ou affichage par transmission vers la vue.
// On charge le modele de connexion à la BDD avec require_once pour éviter les erreurs dues à la redéclaration de fonctions.
require_once("modeleBdd.php");
require_once("modeleSession.php");
require_once("helpersForm.php");

/*
function getActualites()
Fonction retournant la liste des actualités
*/
function getActualites() {
  $db = connect();
  // Code de récupération de notre liste d'actualités
  // 1 - écriture de la requête SQL SELECT : On fait une jointure entre crud_utilisateur et crud_actu.
  // Premier exemple de jointure avec la clause Where
  $sql = "SELECT titre_actu, texte_actu, nom_utilisateur, prenom_utilisateur FROM
  crud_actu, crud_utilisateur
  WHERE crud_actu.id_utilisateur = crud_utilisateur.id_utilisateur";

  // Deuxième exemple de jointure avec INNER JOIN
  $sql = "SELECT titre_actu, texte_actu, nom_utilisateur, prenom_utilisateur FROM
  crud_actu
  INNER JOIN crud_utilisateur
  ON crud_actu.id_utilisateur = crud_utilisateur.id_utilisateur";

  // troisième exemple de jointure avec JOIN et des ALIAS
  $sql = "SELECT actu.titre_actu, actu.texte_actu, actu.id_actu, util.nom_utilisateur, util.prenom_utilisateur, util.id_utilisateur, util.img2_utilisateur, cat.id_cat, cat.nom_cat, actu.date_modif FROM
  crud_actu AS actu
  LEFT JOIN crud_utilisateur AS util
  ON actu.id_utilisateur = util.id_utilisateur
  LEFT JOIN crud_cat AS cat
  ON actu.id_cat = cat.id_cat
  ORDER BY actu.date_modif DESC";

  // 2 - Envoi de la requête avec la méthode try catch
  try {
    $retour["donnees"] = $db->query($sql);
    $retour["statut"] = "ok";
  } catch (PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }
  return $retour;
}

function getActualitesRecentes() {

  // 1 - récupération de la date actuelle
  $aujourdhui = new DateTime();
  // 2 - Création d'un interval négatif de 2 mois
  $interval = DateInterval::createFromDateString("- 2 months");
  // 3 - ajout de l'interval à ma date de départ
  $dateDebut = $aujourdhui->add($interval);
  // 4 - formatage de la date pour une bonne compatibilité SQL.
  $dateDebutFormatee = $dateDebut->format("Y-m-d H:i:s");


  $db = connect();

  // troisième exemple de jointure avec JOIN et des ALIAS
  $sql = "SELECT actu.titre_actu, actu.texte_actu, actu.id_actu, util.nom_utilisateur, util.prenom_utilisateur, util.id_utilisateur, util.img2_utilisateur, cat.id_cat, cat.nom_cat, actu.date_modif FROM
  crud_actu AS actu
  LEFT JOIN crud_utilisateur AS util
  ON actu.id_utilisateur = util.id_utilisateur
  LEFT JOIN crud_cat AS cat
  ON actu.id_cat = cat.id_cat
  WHERE actu.date_modif > '".$dateDebutFormatee."'
  ORDER BY actu.date_modif DESC";

  // 2 - Envoi de la requête avec la méthode try catch
  try {
    $retour["donnees"] = $db->query($sql);
    $retour["statut"] = "ok";
  } catch (PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }
  return $retour;
}


/*
function listeActualites
Permet de retourner la liste des actualités
*/
function listeActualites() {
  $contenu = array();
  $contenu["titre"] = "Actualités";
  $contenu["corps"] = "";
  $donnees = getActualites();
  if($donnees["statut"] == "ok") {
    $contenu["corps"] = $donnees["donnees"];
  } else {
    throw new Exception ($donnees["donnees"]);
  }
  return $contenu;
}


/*
deleteActualite
Cette fonction permet de supprimer une actualité
*/
function deleteActualite() {
  // instanciation d'une connexion PDO
  $db = connect();

  // Si on n'a pas d'id en GET, on redirige vers la page de listing.
  if(empty($_GET["id"])) {
    header("location:index.php?page=actualites");
    exit;
  }
  $id = $_GET["id"];

  // On vérifie que l'utilisateur qu'on veut supprimer est bien le notre si on n'est pas admin.
  if($_SESSION["utilisateur"]["role"] != 9) {
    verifUserId($id);
  }

  // Requête de suppression d'une actualité ciblée par son id
  // 1 - écriture de la requête SQL DELETE.
  $sql = "DELETE FROM crud_actu WHERE id_actu= :id";
  // 2 - Envoi de la requête avec la méthode try catch
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':id', $_GET["id"], PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();

    // Si on est pas admin, on atterit sur l'accueil
    if($id == $_SESSION["utilisateur"]["id"]) {
      header("location:index.php?mess=delete2");
      exit;
    } else {
      header("location:index.php?page=actualites&mess=delete1");
      exit;
    }

  } catch (PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }
}


/*
Fonction createActualite()
Cette fonction permet de gérer les infos relatives à la page de création d'une nouvelle actu
*/
function createActualite() {
  if($_SESSION["utilisateur"]["role"] != 9) {
    header("location:index.php?page=utilisateur");
    exit;
  }
  // On définit le titre de la page
  $retour["titre"] = "Création d'une nouvelle actualité";

  // Définition des variables des données du formulaire (par défaut, tout est vide)
  $donnees["donnees"]["id_actu"] = "NULL";
  $donnees["donnees"]["titre_actu"] = "";
  $donnees["donnees"]["texte_actu"] = "";
  $donnees["donnees"]["id_utilisateur"] = "";

  // Si on vient de soumettre le formulaire, on passe dans la condition suivante
  if($_SERVER["REQUEST_METHOD"] == "POST") {
    // On appelle la fonction verifFormulaire, qui va analyser les données transmises en POST et qui retourne un tableau composé de deux sous-éléments :
    // - les erreurs sous la forme d'un sous tableau
    // - Les données validées sous la forme d'un sous tableau
    $donnees = verifFormulaireActus();
    // si le tableau d'erreur est vide, c'est que le formulaire est validé
    if(empty($donnees["erreur"])) {
      // On utilise une structure try catch pour lancer notre requête d'insertion.
      try {
        // on appelle la fonction saveUtilisateur avec comme paramètres notre tableau de données renvoyé par la fonction verifFormulaire et avec le paramètre insert
        saveActualite($donnees["donnees"], "insert");
        //si l'insertion s'est bien effectuée, on redirige vers la page de listing des actualités
        header("location:index.php?page=actualites&mess=insert1");
      } catch(Exception $e) {
        // s'il y a eu un problème dans l'insertion on attrape l'exception et on en renvoie une nouvelle avec le même message à notre fonction appelante (dans le contrôleur).
        throw new Exception ($e->getMessage());
      }

    } else {
      // si on a des erreurs dans notre vérification du formulaire, on transforme le sous-tableau en chaîne avec implode.
      $donnees["erreur"] = implode("<br />", $donnees["erreur"]);
      $donnees["erreur"] = "<p class='erreur'>".$donnees["erreur"]."</p>";
    }
  }
  // on retourne les données du formulaire à travers le sous-tableau $retour["corps"]
  $retour["corps"] = $donnees;
  return $retour;
}

/*
function updateActualite()
Fonction de mise à jour des données d'une actu
*/
function updateActualite() {
  // On définit un titre pour la page
  $retour["titre"] = "Mise à jour de l'actualité";

  // Si on n'a pas d'id en GET, on redirige vers la page de listing.
  if(empty($_GET["id"])) {
    header("location:index.php?page=actualites");
    exit;
  }
  $id = intval($_GET["id"]);

  // On vérifie que le membre qu'on veut mettre à jour est bien le notre si on n'est pas admin.
  if($_SESSION["utilisateur"]["role"] != 9) {
    // on appella fonction verifUserId qui va arrêter l'exécution du code si l'id en GET n'est pas le même que l'id en SESSION.
    // verifUserId($id);
    /* CODE A INSERER POUR VERIFIER LA PROPRIETE DE L'ACTU */
  }


  if($_SERVER["REQUEST_METHOD"] == "POST") {
    $donnees = verifFormulaireActus();
    // si le tableau d'erreur est vide, c'est que le formulaire est validé
    if(empty($donnees["erreur"])) {
      // On utilise une structure try catch pour lancer notre requête d'insertion.
      try {
        // on appelle la fonction saveUtilisateur avec comme paramètres notre tableau de données renvoyé par la fonction verifFormulaire et avec le paramètre insert
        saveActualite($donnees["donnees"], "update");
        //si l'insertion s'est bien effectuée, on redirige vers la page de listing des actualités
        header("location:index.php?page=actualites&mess=update1");
        exit;

      } catch(Exception $e) {
        // s'il y a eu un problème dans l'insertion on attrape l'exception et on en renvoie une nouvelle avec le même message à notre fonction appelante (dans le contrôleur).
        throw new Exception ($e->getMessage());
      }

    } else {
      // si on a des erreurs dans notre vérification du formulaire, on transforme le sous-tableau en chaîne avec implode.
      $donnees["erreur"] = implode("<br />", $donnees["erreur"]);
      $donnees["erreur"] = "<p class='erreur'>".$donnees["erreur"]."</p>";
      $retour["corps"]["erreur"] = $donnees["erreur"];
      $retour["corps"]["donnees"] = $donnees["donnees"];
    }
  } else {
    try {
      $donnees = getDetailsActualite($id);
      $retour["corps"]["donnees"] = $donnees;
    } catch(Exception $e) {
      throw new Exception ($e->getMessage());
    }
  }

  return $retour;
}


/*
function getDetailsActualite()
@param int $id: id de l'actualite recherchée
@param string $mode : si "details" alors requete avec jointure complete
Cette fonction retourne toutes les infos d'une actualité
*/
function getDetailsActualite($id, $mode="") {
  // nettoyage de l'identifiant :
  $id = intval($id);
  // instanciation de connexion PDO
  $db = connect();
  // On récupère les données de l'actu dont l'identifiant est passé en paramètre.
  // Requête SQL SELECT avec utilisation des alias pour renommer les noms des colonnes afin de simplifier la manipulation des données dans la page.
  if($mode == "") {
    $sql = "SELECT id_actu, titre_actu, texte_actu, id_utilisateur
    FROM crud_actu WHERE id_actu = :id";
  } else if ($mode == "details") {
    $sql = "SELECT id_actu, titre_actu, texte_actu, ACTU.id_utilisateur, ACTU.id_cat, nom_cat, nom_utilisateur, prenom_utilisateur, login_utilisateur, date_creation, date_modif
    FROM crud_actu AS ACTU
    LEFT JOIN crud_cat AS CAT ON ACTU.id_cat = CAT.id_cat
    LEFT JOIN crud_utilisateur AS UTIL ON ACTU.id_utilisateur = UTIL.id_utilisateur
    WHERE id_actu = :id";
  }

  // 2 - Envoi de la requête avec la méthode try catch
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':id', $id, PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();
    // Je récupère l'ensemble des données retournées par la requête grâce à fetchAll
    $utilisateur = $req->fetchAll()[0];
    // j'assigne ces données à mes variables utilisées dans mon formulaire
    return $utilisateur;

  } catch (PDOException $erreur) {
    throw new Exception ($erreur->getMessage());
  }
}

/*
fonction permettant de retourner les illustrations assignées à une actu
*/
function getIllustrationsByActualite($id) {
  // nettoyage de l'identifiant :
  $id = intval($id);
  // instanciation de connexion PDO
  $db = connect();
  // Requête SQL
  $sql = "SELECT rel.id_illus, url_illus, texte_illus
  FROM crud_illus_actu AS rel JOIN crud_illus AS illu
  ON rel.id_illus = illu.id_illus
  WHERE rel.id_actu = :idActu";

  // 2 - Envoi de la requête avec la méthode try catch
  try {
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':idActu', $id, PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();
    // Je récupère l'ensemble des données retournées par la requête grâce à fetchAll
    $listeIllus = $req->fetchAll();
    // j'assigne ces données à mes variables utilisées dans mon formulaire
    return $listeIllus;

  } catch (PDOException $erreur) {
    throw new Exception ($erreur->getMessage());
  }
}

/*
function verifFormulaireActus
Fonction vérifiant les champs du formulaire.
Retourne un tableau composé de deux sous-tableaux :
- erreur : contient les éventuelles erreurs sur les champs
- donnees : contient les données validées de chaque champ
*/
function verifFormulaireActus() {
  $retour = array(
    "erreur" => array(),
    "donnees" => array()
  );

  $retour["donnees"]["id"] = $_POST["id"];

  if(!$retour["donnees"]["titre_actu"] = verifTexte($_POST["titre_actu"], 255)) {
    $retour["erreur"]["titre_actu"] = "La longueur du titre de l'actualité doit être comprise entre 0 et 255 caractères.";
  }
  if(!$retour["donnees"]["texte_actu"] = verifTexteMin($_POST["texte_actu"], 100)) {
    $retour["erreur"]["texte_actu"] = "La longueur de votre texte doit être au minimum de 100 caractères.";
  }
  if(empty($_POST["id_utilisateur"])) {
    $retour["erreur"]["id_utilisateur"] = "Merci de préciser un auteur";
  } else {
    $retour["donnees"]["id_utilisateur"] = $_POST["id_utilisateur"];
  }

  return $retour;
}

/*
Fonction saveActualité
@param array $donnees : tableau des données relatives à l'actualité
@param string $mode : le mode d'utilisation de la fonction : en insert ou en update
Cette fonction a pour rôle d'enregistrer un utilisateur dans la BDD, que ce soit en insert ou en update
*/
function saveActualite($donnees, $mode) {
  // on commence par instancier une nouvelle connexion PDO
  $db = connect();

  $role = "0";

  // On teste si le tableau $donnees n'est pas conforme à nos attentes (s'il est vide, ou si ce n'est pas un tableau), si c'est le cas, on envoie une nouvelle exception.
  if(empty($donnees) || !is_array($donnees)) {
    throw new Exception("Les données à enregistrer ne sont pas valides !");
  }
  // On teste le deuxième paramètre de notre fonction ($mode), qui doit avoir deux valeurs possibles : "insert" ou "update".
  // En fonction de ces valeurs, on va écrire une requête SQL différente.
  if($mode == "insert") {
    // requete SQL d'insertion
    $sql = "INSERT INTO crud_actu VALUES (:id, :titre, :texte, :auteur, 1, NOW() , NOW() )";
  } else if ($mode == "update") {
    // requete SQL d'update
    $sql = "UPDATE crud_actu
    SET titre_actu = :titre, texte_actu = :texte, id_utilisateur = :auteur, id_cat = 1, date_modif = NOW()
    WHERE id_actu = :id";
  } else {
    // si la valeur de $mode n'est pas conforme, on envoie une nouvelle exception.
    throw new Exception ("le mode d'utilisation de la fonction n'est pas reconnu !");
  }

  // On utilise un try catch pour lancer la requête PDO.
  try {
    if($donnees["id"] == "NULL") {
      $donnees["id"] = null;
    }
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':titre', $donnees["titre_actu"], PDO::PARAM_STR);
    $req->bindParam(':texte', $donnees["texte_actu"], PDO::PARAM_STR);
    $req->bindParam(':auteur', $donnees["id_utilisateur"], PDO::PARAM_INT);
    $req->bindParam(':id', $donnees["id"]);
    // Exécution de la requête
    $req->execute();
  } catch (PDOException $erreur) {
    // si la requête PDO échoue, on attrape l'exception de type PDOException et on envoie une nouvelle exception en reprenant le même message.
    throw new Exception (print_r($donnees)."<br />".$erreur->getMessage());
  }
}


function getLienActualite($titre, $id) {
  $titre = wd_remove_accents($titre);
  $lien = URL."/article/".$titre."-".$id;
  return $lien;
}

function wd_remove_accents($str, $charset='utf-8') {

  $str = htmlentities($str, ENT_NOQUOTES, $charset);
  $str = iconv('UTF-8','ASCII//TRANSLIT',$str);
  $str = preg_replace('#&([A-za-z])(?:acute|nacute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
  $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
  $str = str_replace(' ', "-", $str);
  $str = str_replace("'", "-", $str);
  $str = strtolower($str);
  $str = preg_replace('#\(|\)|\|#', '', $str);
  $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

  return urlencode($str);
}

/*
Fonction supprimant le lien existant entre une actualité et une illustration
*/
function enleveIllus($idActu, $idIllus) {
  try {
    $db = connect();
    $sql = "DELETE FROM crud_illus_actu WHERE id_actu = :idActu AND id_illus = :idIllus";
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':idActu', $idActu, PDO::PARAM_INT);
    $req->bindParam(':idIllus', $idIllus, PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();
    header("location:index.php?page=actualites&action=update&id=".$idActu);
    exit;
  } catch (PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }
}

/*
Fonction ajoutant une image d'illustration à une actu
*/
function ajouteIllus($idActu, $idIllus) {
  try {
    $db = connect();
      $sql = "INSERT INTO crud_illus_actu VALUES (:idIllus, :idActu)";
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':idActu', $idActu, PDO::PARAM_INT);
    $req->bindParam(':idIllus', $idIllus, PDO::PARAM_INT);
    // Exécution de la requête
    $req->execute();
    header("location:index.php?page=actualites&action=update&id=".$idActu);
    exit;
  } catch (PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }
}

/*
functions searchActualitesByTitre()
@param string $titre
Fonction permettant de rechercher les actualités correspondantes à la recherche $titre.
*/
function searchActualitesByTitre($titre) {
  // on instancie une variable $retour de type array à vide
  $retour = array();
  // on stocke dans notre variable $titre le paramètre de recherche qui va être utilisé dans notre requête SQL.
  // Pour cela on utilise la syntaxe %recherche% qui permet de retourner toutes les données contenant notre recherche
  $titre = "%".$titre."%";
  try {
    $db = connect();
    // on écrit la requête SQL de la recherche
    $sql = "SELECT id_actu, titre_actu, texte_actu FROM crud_actu WHERE titre_actu LIKE :titre";
    // On prépare la requête : elle est envoyée au serveur sans les données variables
    $req = $db->prepare($sql);
    // On lie la donnée récupérée en GET avec notre requête préparée, et on déclare qu'elle doit être un entier.
    $req->bindParam(':titre', $titre, PDO::PARAM_STR);
    // Exécution de la requête
    $req->execute();
    // on stocke dans $nombre le nombre de lignes retournées par la requête.
    $nombre = $req->rowCount();
    // si on a plus d'un résultat
    if($nombre>1) {
      // on ajoute un s
      $nombre .= " résultats";
    } else {
      $nombre .= " résultat";
    }
    // On utilise notre variable $retour pour retourner le résultat de notre requête.
    $retour["titre"] =  "Votre recherche a produit ".$nombre;
    $retour["donnees"] = $req;
    return $retour;
  } catch (PDOException $erreur) {
    throw new Exception($erreur->getMessage());
  }

}

/*
Fonction permettant de générer un PDF à partir d'une actualité.
*/
function generationPDFActualite($id) {
  $idActu = intval($id);

  // Récupération des données de l'actualité
  $actu = getDetailsActualite($idActu);
  $listeIllus = getIllustrationsByActualite($_GET["id"]);

  $titreActu = $actu["titre_actu"];
  $texteActu = $actu["texte_actu"];
  // on ajoute des BR pour revenir à la ligne dans le PDF
  $texteActu = wordwrap($texteActu, 80, '<br />', true);

  $illustrations = "";
  // récupération des illustrations
  foreach ($listeIllus as $illus) {
    $illustrations .= "<td style='width: 30%;'><img src='".URL."/uploads/illustrations/thumb-".$illus["url_illus"]."' style='width: 3cm; height'/></td>";
  }

  $nom = "Crud-".$actu["titre_actu"].".pdf";

  // génération du QRCode renvoyant sur l'URL de l'article
  $flashCode = '<qrcode value="'.getLienActualite($titreActu, $idActu).'" ec="H" style="width: 50mm; background-color: white; color: black;"></qrcode>';

  $html = "
  <style>

  </style>
  <page>
    <table>
      <tr>
        <td style='width: 6cm; color: #ff0000; font-size: 20pt; vertical-align: top;'><span>".$titreActu."</span></td>
        <td style='width: 14cm; color: #444; font-size: 12pt; line-height: 18pt; text-align: left;'>
        <p style='width: 100pt; text-align: justify; word-wrap: break-word;'><br />".$texteActu."</p></td>
      </tr>
      <tr>
        <td style='width: 6cm; font-size: 14pt; font-weight: bold;'>FLASHCODE<br />".$flashCode."</td>
        <td style='width: 14cm;'>
          <table>
            <tr>
              ".$illustrations."
            </tr>
          </table>
        </td>
      </tr>

    </table>
  </page>
  ";

  // on instancie un nouvel objet de la classe HTML2PDF
  $pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'fr', true, 'UTF-8');
  $pdf->setDefaultFont('freeserif');

  $pdf->writeHTML($html);

  $enregistrement = "/exports/pdf/".$nom;
  // enregistrement
  $pdf->output($enregistrement, 'F');
  // affichage
  $pdf->output($nom);

}


?>
