<?php
// Ce modèle va servir à gérer l'instance de connexion à la BDD.

/*
fonction connect() permettant de retourner une instance de la classe PDO, qui sera utilisée pour toutes nos requêtes.
*/
function connect() {
  // Déclaration des variables de connexion à la BDD
  $user = "root";
  $pass = "";
  // On va utiliser la structure try {} catch pour instancier notre connexion à la base.
  // Cette structure permet d'intercepter les erreurs PDO et de les traiter selon nos propres règles de gestion.
  try {
    // Instanciation de notre objet PDO qui va gérer la connexion vers la BDD.
    $db = new PDO('mysql:host=localhost;dbname=crud;charset=UTF8', $user, $pass);
    // Définition du mode de gestion d'erreur de PDO : En cas d'erreur, PDO va envoyer des erreurs de type PDOException permettant une gestion plus fine de ces erreurs.
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // si la connexion a fonctionné, $db est retourné par la fonction.
    return $db;
  } catch (PDOException $erreur) {
    die("Problème : ".$erreur->getMessage());
  }
}
