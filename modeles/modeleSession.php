<?php
require_once("modeleUtilisateurs.php");// Fonctions permettant de gérer les sessions

/*
Fonction permettant de vérifier si l'utilisateur est connecté.
Si ce n'est pas le cas, ou s'il y a un problème avec son token, on exécute la fonction deconnexion()
*/
function verifSession() {
  // Si on n'a pas de session ou si $_SESSION["log"] est différent de "OK" ou si le token en Session n'est pas le même que celui généré (vol de session), on déconnecte.
  if(!isset($_SESSION["log"]) || $_SESSION["log"] != "OK" || $_SESSION["token"] != generationToken()) {
    deconnexion();
  }
}

/*
Fonction permettant de générer une signature (token) propre à chaque utilisateur en fonction de variables d'environnement de sa connexion (adresse IP et user Agent).
*/
function generationToken() {
  $adresseIP = $_SERVER["REMOTE_ADDR"];
  $userAgent = $_SERVER['HTTP_USER_AGENT'];
  $token = $adresseIP.$userAgent;
  return md5($token);
}

/*
Fonction permettant de déconnecter un utilisateur en supprimant sa session et en vidant les variables liées.
*/
function deconnexion($mess = "prive") {
  session_destroy();
  $_SESSION = array();
  header("location:index.php?mess=".$mess);
  exit;
}

/*
Fonction getUserSession()
Permet de retourner les infos de Session formatées
*/
function getUserSession() {
    $user["id"] = $_SESSION["utilisateur"]["id"];
    $user["nom"] = ucfirst($_SESSION["utilisateur"]["nom"]);
    $user["prenom"] = ucfirst($_SESSION["utilisateur"]["prenom"]);
    $user["age"] = $_SESSION["utilisateur"]["age"];
    $user["ageFormat"] = $_SESSION["utilisateur"]["age"]." ans";
    $user["classe"] = $_SESSION["utilisateur"]["classe"];
    $user["role"] = $_SESSION["utilisateur"]["role"];
    $user["roleFormat"] = afficheRole($_SESSION["utilisateur"]["role"]);
    return $user;
}

?>
