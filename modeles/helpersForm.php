<?php
// Dans ce fichier seront déclarées les fonctions utilitaires de validation des champs des formulaires

/*
function verifTexte vérifie que $donnee n'est pas vide et est inférieur ou égal à $longueur
@param string $donnee
@param int $longueur
*/
function verifTexte($donnee, $longueur=46) {
  if(empty($donnee) || strlen($donnee) > $longueur) {
    return false;
  } else {
    return htmlspecialchars($donnee);
  }
}

/*
function verifTexteMin vérifie que $donnee n'est pas vide et est supérieur ou égal à $longueur
@param string $donnee
@param int $longueur
*/
function verifTexteMin($donnee, $longueur=20) {
  if(empty($donnee) || strlen($donnee) < $longueur) {
    return false;
  } else {
    return htmlspecialchars($donnee);
  }
}

/*
function verifAge vérifie que $donnee n'est pas vide et est compris entre 17 et 127
@param string $donnee
*/
function verifAge($donnee) {
  if(empty($donnee) || $donnee > 127 || $donnee < 17) {
    return false;
  } else {
    return intval($donnee);
  }
}

/*
function verifPass vérifie que $donnee n'est pas vide et est compris entre 8 et 50 et retourne $donnee hashé via password_verify
@param string $donnee
*/
function verifPass($donnee) {
  if(empty($donnee) || strlen($donnee) < 8 || strlen($donnee) > 50 ) {
    return false;
  } else {
    return password_hash($donnee, PASSWORD_DEFAULT);
  }
}

/*
function verifMail vérifie que $donnee n'est pas vide que $donnee est bien une adresse mail valide.
@param string $donnee
*/
function verifMail($donnee) {
  if(empty($donnee) || !filter_var($donnee, FILTER_VALIDATE_EMAIL)) {
    return false;
  } else {
    return htmlspecialchars($donnee);
  }
}

/*
Fonction de gestion de l'upload des photos de profil
@param string : $nomInput : le nom du champ input gérant la photo
@param int : $tailleMax : la taille max en Mo autorisée
@param bool : $obligatoire : si true, la photo est obligatoire, si false, elle ne l'est pas.
*/
function verifPhoto($nomInput, $obligatoire = true, $tailleMax = 6) {
  $retour = array();
  $tailleMaxAffichee = $tailleMax." Mo";
  // Liste MIME images
    $listeMime = array(
      ".jpeg" => "image/jpeg",
      ".jpg" => "image/jpg",
      ".gif" => "image/gif",
      ".png" => "image/png"
    );
    if(empty($_FILES)) {
      // Le tableau $_FILES est complètement vide (on n'a même pas de sous-tableau correspondant à l'input) : On a essayer de télécharger un fichier dépassant la valeur limite du paramètre post_max_size défini dans php.ini
      $retour["valid"] = false;
      $retour["message"] = "Le fichier téléchargé est trop lourd, merci d'envoyer un fichier de moins de $tailleMaxAffichee.";
    } else {
      // on continue les tests en regardant les messages d'erreurs renvoyés par PHP.
      $fileErreur = $_FILES["$nomInput"]["error"];
      switch($fileErreur) {
        case '1':
          // La taille du fichier téléchargé excède la valeur de upload_max_filesize, configurée dans le php.ini.
          $retour["valid"] = false;
          $retour["message"] = "Le fichier téléchargé est trop lourd, merci d'envoyer un fichier de moins de $tailleMaxAffichee.";
          break;
        case '2':
          // La taille du fichier téléchargé excède la valeur de MAX_FILE_SIZE, qui a été spécifiée dans le formulaire HTML.
          $retour["valid"] = false;
          $retour["message"] = "Le fichier téléchargé est trop lourd, merci d'envoyer un fichier de moins de $tailleMaxAffichee.";
          break;
        case '3':
          // Le fichier n'a été que partiellement téléchargé.
          $retour["valid"] = false;
          $retour["message"] = "Un problème est survenu lors du téléchargement. Merci de réessayer.";
          break;
        case '4':
          // Aucun fichier n'a été téléchargé.
          if($obligatoire == true) {
            $retour["valid"] = false;
            $retour["message"] = "Aucune photo de profil n'a été téléchargée.";
            break;
          } else { // si la photo n'est pas obligatoire
            $retour["valid"] = true;
            $retour["message"] = "no photo.";
            break;
          }

        case '6':
    			// Un dossier temporaire est manquant. Introduit en PHP 5.0.3.*
          $retour["valid"] = false;
          $retour["message"] = "Le téléchargement a échoué. Merci de prendre contact avec l'administrateur du site.";
    			break;
        case '7':
          // Échec de l'écriture du fichier sur le disque. Introduit en PHP 5.1.0.
          $retour["valid"] = false;
          $retour["message"] = "Le téléchargement a échoué. Merci de prendre contact avec l'administrateur du site.";
          break;
        case '8';
          // Une extension PHP a arrêté l'envoi de fichier
          $retour["valid"] = false;
          $retour["message"] = "Le téléchargement a échoué. Merci de prendre contact avec l'administrateur du site.";
          break;

        default; // équivaut à un code erreur 0

        // Analyse du type MIME du fichier
        // On va regarder si le type MIME du fichier correspond bien à ce qu'on attend. On pourrait être tenté d'utiliser l'élément type du sous-tableau dans $_FILES mais celui-ci est facilement contournable en changeant simplement l'extension du fichier.
        // On va utiliser une méthode plus efficace pour récupérer le type MIME réel du fichier.
        // On récupère l'adresse du fichier temporaire qu'on vient d'uploader
        $tmpFile = $_FILES[$nomInput]["tmp_name"];
        // on ouvre une nouvelle ressource fileinfo
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        // on récupère le type MIME réel du fichier.
        $typeMime = finfo_file($finfo, $tmpFile);
        // on referme la ressource fileinfo.
        finfo_close($finfo);

        // on vérifie que notre type MIME est présent dans notre tableau
        if(!in_array($typeMime, $listeMime)) {
          // type MIME non accepté
          $retour["valid"] = false;
          $retour["message"] = "Le format de votre fichier n'est pas accepté !";
          break;
        } else {
          // le type MIME est accepté ! On récupère l'extension liée.
          $extension = array_search($typeMime, $listeMime);
          $retour["valid"] = true;
          $retour["ext"] = $extension;
          $retour["mime"] = $typeMime;
        }
    }
    return $retour;
  }
}

?>
