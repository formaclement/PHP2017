<?php
// Ce modèle sert à parser un flux RSS.

/*
function afficheRSS
Fonction permettant de parser un flux RSS et de l'afficher sur la page
@param string $url : l'url du flux RSS
@param $nbArticles : le nombre d'articles à afficher.

*/
function afficheRSS($url, $nbArticles) {
  $retour = "";

  // on charge notre flux RSS
  $source = new SimpleXMLElement($url, NULL, true);

  // On récupère la liste des articles en ciblant la balise item
  $listeArticles = $source->channel->item;
  // on initialise un compteur
  $i = 0;
  // on parcourt les articles avec un foreach
  foreach ($listeArticles as $article) {
    if($i<$nbArticles) {
      // on récupère le tire
      $titre = $article->title;
      // on récupère le lien
      $lien = $article->link;
      // on récupère la date qu'on formate en français grâce à Intl
      $format = new IntlDateFormatter("fr_FR", IntlDateFormatter::FULL, IntlDateFormatter::NONE);
      $publication = $article->pubDate;
      $dateTimestamp = strtotime($publication);
      $publication = $format->format($dateTimestamp);
      // On récupère le contenu de l'extrait
      $contenu = $article->description;
      // Comme celui-ci contient du HTML dans une balise CDATA, on crée à partir de lui un nouvel objet de type DOMDocument.
      // Ceci va nous permettre de le parser pour en extraire l'image.

      $htmlParser = new DOMDocument();
      // load the HTML:
      $htmlParser->loadHTML($contenu);
      // import it into simplexml:
      $html = simplexml_import_dom($htmlParser);
      // On récupère le src et le alt de l'image.
      $vignetteSrc = $html->body->img["src"];
      $vignetteAlt = $html->body->img["alt"];

      // A chaque tour de boucle on ajoute un nouvel élément html contenant les infos issues du RSS.
      $retour .= "
       <a href='".$lien."' class='rss'>
         <h5>".$titre."</h5>
         <span>".$publication."</span>
         <img src='".$vignetteSrc."' alt='".$vignetteAlt."' />
       </a>
      ";
    }
   $i++;
  }
  // on retourne l'ensemble HTML complet.
  return $retour;
}


?>
