<?php
require_once("modeles/modeleActualites.php");
require_once("modeles/modeleMessages.php");
require_once("modeles/modeleUtilisateurs.php");
require_once("modeles/modeleRss.php");
$message = getAutomatiques();



try {
  $page = getActualitesRecentes();
  $listeUtilisateurs = getUtilisateursGeoloc();
  $rss = afficheRSS("http://industries-creatives.com/feed/", 4);
} catch (Exception $e) {
  $probleme = $e;
  require("vues/vueProbleme.php");
}

$page ["titre"] = "Bienvenue sur le site !";

require("vues/vueAccueil.php");
?>
