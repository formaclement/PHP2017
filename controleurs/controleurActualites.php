<?php
require_once("modeles/modeleUtilisateurs.php");
require_once("modeles/modeleIllustrations.php");
require_once("modeles/modeleActualites.php");
require_once("modeles/modeleCategories.php");
require_once("modeles/modeleMessages.php");
require_once("modeles/modeleSession.php");
require_once("modeles/modelePhoto.php");
$user = getUserSession();
// Le fichier de controleur va analyser la requête HTTP. En fonction des paramètres de cette requête il va lancer les fonctions correspondantes dans notre modèle.
// Après l'appel du modèle et les appels de fonctions, le controleur va charger la vue correspondante.
$message = getAutomatiques();

// action par défaut : affichage de la liste de tous les utilisateurs
try {
  if(!empty($_GET["action"])) {

    if($_GET["action"] == "create") {
      $utilisateurs = getUtilisateurs();
      $page = createActualite();
      require("vues/vueActualitesDetails.php");
    } else if ($_GET["action"] == "delete") {
      $page = deleteActualite();
    } else if ($_GET["action"] == "update") {
      $listeIllus = getIllustrationsByActualite($_GET["id"]);
      $mediatheque = listeIllustrations();
      $utilisateurs = getUtilisateurs();
      $page = updateActualite();
      require("vues/vueActualitesDetails.php");
    } else if($_GET["action"] == "generationPDF") {
      $page = generationPDFActualite($_GET["id"]);
    }
    else {
      $probleme = "L'action que vous souhaitez effectuer n'existe pas sur le site.";
      require("vues/vueProbleme.php");
    }

  } else {
    if($user["role"] != 9) {
      header("location:index.php?page=utilisateurs&action=update&id=".$user["id"]);
      exit;
    } else {
      $listeCats = getCategories();
      $page = listeActualites();
      require("vues/vueActualites.php");
    }
  }
} catch (Exception $e) {
  $probleme = $e;
  require("vues/vueProbleme.php");
}

?>
