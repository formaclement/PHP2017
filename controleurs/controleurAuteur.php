<?php
require_once("modeles/modeleUtilisateurs.php");
require_once("modeles/modeleMessages.php");

$message = getAutomatiques();

try {
  if(!empty($_GET["login"])) {
    $page = getDetailsUtilisateurByLogin($_GET["login"], "details");
    $img = getImageUtilisateurByLogin($_GET["login"], "large");
    $page["titre"] = $page["nom"]." ".$page["prenom"];
    if(!isset($_GET["mode"])) {
      if(file_exists("vues/vueAuteur-".$_GET["login"].".php")) {
        require_once("vues/vueAuteur-".$_GET["login"].".php");
      } else {
        // chargement de la vue normale
        require_once("vues/vueAuteur.php");
      }
    } else {
      // chargement de la vue Ajax
      require_once("vues/ajax/vueAuteur.php");
    }


  } else {
    throw new Exception("Auteur non trouvé !");
  }

} catch(Exception $e) {
  $probleme = $e;
  require("vues/vueProbleme.php");
}

 ?>
