<?php
// Controleur en charge de l'affichage des articles sur le front.
require_once("modeles/modeleActualites.php");
require_once("modeles/modelePhoto.php");
require_once("modeles/modeleMessages.php");

$message = getAutomatiques();


try {
  if(!empty($_GET["id"])) {
    // Récupération de l'article par son id
    $listeIllus = getIllustrationsByActualite($_GET["id"]);
    $page = getDetailsActualite($_GET["id"], "details");
    $page["titre"] = $page["titre_actu"];
    require_once("vues/vueArticle.php");
  } else {
    throw new Exception("Contenu non trouvé !");
  }
} catch (Exception $e) {
  $probleme = $e;
  require("vues/vueProbleme.php");
}
?>
