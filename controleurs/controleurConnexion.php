<?php
// Controleur permettant de gérer la connexion au site
require_once("modeles/modeleUtilisateurs.php");
require_once("modeles/modeleMessages.php");

$message = getAutomatiques();

if(!empty($_GET["action"])) {
  if($_GET["action"] == "deco") {
    $page = deconnexion("sess0");
  } else if($_GET["action"] == "recup") {
    $page = recupPass();
    require("vues/vueConnexionRecup.php");
  }
} else {
  $page = connexion();
  require("vues/vueConnexion.php");
}



?>
