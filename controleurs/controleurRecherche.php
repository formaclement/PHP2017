<?php
// On charge les modèles des actualités et des messages
require_once("modeles/modeleActualites.php");
require_once("modeles/modeleMessages.php");
// on appelle la fonction getAutomatiques et on stocke son résultat dans $message
$message = getAutomatiques();

// on utilise la structure try catch pour récupérer les exceptions que peuvent lancer nos fonctions
try {
  // on regarde si on a une valeur pour $_GET["search"]
  if(!empty($_GET["search"])) {
    // on stocke dans $page le résultat de la fonction searchActualitesByTitre() avec en paramètre $_GET["search"]
    $page = searchActualitesByTitre($_GET["search"]);
    // On regarde si on a une valeur pour $_GET["mode"] et si cette valeur est égale à "ajax"
    if(!empty($_GET["mode"]) && $_GET["mode"] == "ajax") {
      // si c'est le cas, on charge la vue ajax
      require_once("vues/ajax/VueRecherche.php");
    } else {
      // sinon la vue par défaut.
      require_once("vues/VueRecherche.php");
    }
  } else {
    // si ce n'est pas le cas, on redirige vers l'index.
    header("location:index.php");
    exit;
  }
} catch(Exception $e) { // on récupère l'exception dans une variable $probleme
  $probleme = $e;
  // on affiche la vue problème
  require("vues/vueProbleme.php");
}

 ?>
