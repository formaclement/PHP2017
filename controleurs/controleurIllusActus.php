<?php
// Ce controleur va gérer la relation entre les illustrations et les actualités
require_once("modeles/modeleActualites.php");
require_once("modeles/modeleSession.php");
require_once("modeles/modelePhoto.php");

$user = getUserSession();

// action par défaut : affichage de la liste de tous les utilisateurs
try {
  // Je vérifie que j'ai bien une action, un idActu et un idIllus en paramètres GET
  if(!empty($_GET["action"]) && !empty($_GET["idActu"]) && !empty($_GET["idIllus"])) {
    $idActu = intval($_GET["idActu"]);
    $idIllus = intval($_GET["idIllus"]);
    if($_GET["action"] == "create") {
      $page = ajouteIllus($idActu, $idIllus);
    } else if ($_GET["action"] == "delete") {
      $page = enleveIllus($idActu, $idIllus);
    } else {
      $probleme = "L'action que vous souhaitez effectuer n'existe pas sur le site.";
      require("vues/vueProbleme.php");
    }
  } else {
    $probleme = "Url non conforme !";
    require("vues/vueProbleme.php");
  }
} catch (Exception $e) {
  $probleme = $e;
  require("vues/vueProbleme.php");
}
?>
