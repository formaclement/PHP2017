<?php
require_once("modeles/modeleMessages.php");
require_once("modeles/modeleSession.php");
$user = getUserSession();
// Le fichier de controleur va analyser la requête HTTP. En fonction des paramètres de cette requête il va lancer les fonctions correspondantes dans notre modèle.
// Après l'appel du modèle et les appels de fonctions, le controleur va charger la vue correspondante.
$message = getAutomatiques();

$page = array(
  "titre" => "Bienvenue dans votre administration !"
);

require("vues/vuePanel.php");

 ?>
