<?php
// Le controleur commence par charger le ou les modèles qui vont être utilisés dans cette partie du code.
require_once("modeles/modeleUtilisateurs.php");
require_once("modeles/modeleMessages.php");
require_once("modeles/modeleSession.php");
$user = getUserSession();
// Le fichier de controleur va analyser la requête HTTP. En fonction des paramètres de cette requête il va lancer les fonctions correspondantes dans notre modèle.
// Après l'appel du modèle et les appels de fonctions, le controleur va charger la vue correspondante.
$message = getAutomatiques();

// action par défaut : affichage de la liste de tous les utilisateurs
try {
  if(!empty($_GET["action"])) {

    if($_GET["action"] == "create") {
      $page = createUtilisateur();
      require("vues/vueUtilisateursDetails.php");
    } else if ($_GET["action"] == "delete") {
        $page = deleteUtilisateur();
    } else if ($_GET["action"] == "update") {
      $page = updateUtilisateur();
      require("vues/vueUtilisateursDetails.php");
    } else if ($_GET["action"] == "updatePass") {
      echo "Update mot de passe !"; // appel fonction de mise à jour du mot de passe dans le modèle.
    } else if ($_GET["action"] == "suppPhoto") {
      suppressionPhoto();
    } else if ($_GET["action"] == "export") {
      $page = exportUtilisateurs();
    } else {
      $probleme = "L'action que vous souhaitez effectuer n'existe pas sur le site.";
      require("vues/vueProbleme.php");
    }

  } else {
    if($user["role"] != 9) {
      header("location:index.php?page=utilisateurs&action=update&id=".$user["id"]);
      exit;
    } else {
      $page = listeUtilisateurs();
      require("vues/vueUtilisateurs.php");
    }
  }
} catch (Exception $e) {
  $probleme = $e;
  require("vues/vueProbleme.php");
}
