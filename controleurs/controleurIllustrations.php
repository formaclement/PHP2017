<?php
require_once("modeles/modeleIllustrations.php");
require_once("modeles/modeleMessages.php");
require_once("modeles/modeleSession.php");

$user = getUserSession();
// Le fichier de controleur va analyser la requête HTTP. En fonction des paramètres de cette requête il va lancer les fonctions correspondantes dans notre modèle.
// Après l'appel du modèle et les appels de fonctions, le controleur va charger la vue correspondante.
$message = getAutomatiques();

// action par défaut : affichage de la liste de toutes les illustrations
try {
  if(!empty($_GET["action"])) {
    if($_GET["action"] == "create") {
      $page = createIllustration();
    } else if ($_GET["action"] == "delete") {
      $page = deleteIllustration();
    } else if ($_GET["action"] == "update") {
      $page = updateIllustration();
    } else {
      $probleme = "L'action que vous souhaitez effectuer n'existe pas sur le site.";
      require("vues/vueProbleme.php");
    }
  } else {
    if($user["role"] != 9) {
      echo "vous n'êtes pas superAdmin !";
      header("location:index.php?page=utilisateurs&action=update&id=".$user["id"]);
      exit;
    } else {
      $page = listeIllustrations();
    }
  }
  require("vues/vueIllustrations.php");
} catch (Exception $e) {
  $probleme = $e;
  require("vues/vueProbleme.php");
}

?>
