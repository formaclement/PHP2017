<?php
require_once("modeles/modeleCategories.php");
require_once("modeles/modeleMessages.php");
require_once("modeles/modeleSession.php");
$user = getUserSession();
// Le fichier de controleur va analyser la requête HTTP. En fonction des paramètres de cette requête il va lancer les fonctions correspondantes dans notre modèle.
// Après l'appel du modèle et les appels de fonctions, le controleur va charger la vue correspondante.
$message = getAutomatiques();

// action par défaut : affichage de la liste de tous les catégories
try {
  if(!empty($_GET["action"])) {

    if($_GET["action"] == "create") {
      $page = createCategorie();
      if($_SERVER["REQUEST_METHOD"] == "POST") {
        echo json_encode($page);
      } else {
        require("vues/vueCategorieDetails.php");
      }
    } else if ($_GET["action"] == "delete") {
      $page = deleteCategorie();
      echo json_encode($page);
    } else if ($_GET["action"] == "update") {
      $page = updateCategorie();
      if($_SERVER["REQUEST_METHOD"] == "POST") {
        echo json_encode($page);
      } else {
        require("vues/vueCategorieDetails.php");
      }
    } else {
      $probleme = "L'action que vous souhaitez effectuer n'existe pas sur le site.";
      require("vues/vueProbleme.php");
    }

  } else {
    if($user["role"] != 9) {
      echo "vous n'êtes pas superAdmin !";
      // header("location:index.php?page=utilisateurs&action=update&id=".$user["id"]);
      // exit;
    } else {
      header("location:index.php?page=actualites");
    }
  }
} catch (Exception $e) {
  $probleme = $e;
  require("vues/vueProbleme.php");
}

?>
