$(document).ready(function(){

  // Appel ajax du formulaire de création ou de modification d'une catégorie
  $("body").on("click", "a#createCat, a.modifCat", function(event){
    var lien = $(this);
    // on bloque le changement de page
    event.preventDefault();
    // on récupère l'URL du lien
    var urlPage = lien.attr("href");
    // on lance la requête AJAX via Jquery.get qui est un raccourci simplifié pour lancer une requête AJAX de type GET.

    // Création du lien pour l'action du formulaire
    if(typeof($(this).data("id")) != "" && typeof($(this).data("id")) != "undefined") {
      // Si on a cliqué sur le lien de modification
      var urlAction = "index.php?page=categories&action=update&id="+$(this).data("id");
      var mode = "update";
    } else {
      // Si on a cliqué sur le lien de création
      var urlAction = "index.php?page=categories&action=create";
      var mode = "create";
    }

    lien.after("<span id='loader'>Chargement...</span>");


    $.get( urlPage, function(data) { // la fonction passée en paramètre se déclenche au succès et récupère la réponse de la requête à travers la variable data.
      // on insère après notre lien la réponse qui contient le formulaire.
      lien.after(data);
      lien.hide();
      lien.next(".formCat").find("#formCat").attr("action", urlAction);
      lien.next(".formCat").find("#formCat").attr("data-mode", mode);
      $("body").find("#loader").remove();
    })
    .done(function() { // un événement déclenché une fois la requête exécutée sans erreur (inutile dans notre cas)
      console.log( "second success" );
    })
    .fail(function() { // un événement déclenché en cas d'échec de la requête HTTP.
      lien.after("<br />Une erreur est survenue, merci de contacter l'administrateur du site !");
    })
    .always(function() { // un événement se déclenchant systématiquement après la requête (erreur ou succès).
      console.log( "requete AJAX terminée" );
    });

  });


  // on écoute l'événement click sur l'élément HTML a#closeCat pour déclencher la suppression du formulaire.
  // Cet élément étant chargé de manière asynchrone par la requête AJAX, je ne peux pas le cibler de manière classique :
  // $("a#closeCat").click(function(){}); Cet écouteur ne se déclenchera pas.
  // Pour cela il faut que je cible un élément parent présent au moment du chargement de la page. On peut prendre par exemple body.
  // On utilise la fonction jQuery .on qui est un écouteur d'événement plus complet.
  $("body").on("click", "a.closeCat", function(event){
    event.preventDefault(); // j'annule le comportement natif du lien
    $(this).parent().prev().show(); // je réaffiche le lien d'ajout chargeant le formulaire
    $(this).parent().remove(); // je supprime le formulaire.
  });



  // SOUMISSION DU FORMULAIRE D'AJOUT OU DE MODIFICATION DE CATEGORIE
  // J'utilise la fonction .on pour écouter l'événement submit sur le formulaire sans qu'il soit chargé en même temps que la page.
  $("body").on("submit", "form#formCat", function(event){
    event.preventDefault();
    var form = $(this);
    // Je récupère l'URL de traitement du formulaire (contenue dans l'attribut action de la balise)
    var url = form.attr("action");

    // Je lance la requête en utilisant la méthode .post de jQuery.
    $.post(
      url,
      form.serialize(), // Je transmets les données du formulaire grâce à la fonction .serialize
      function(data) { // Je traite la réponse HTTP qui est transmise en JSON
        if(data.statut == "probleme") { // si dans le JSON, statut = probleme
          form.before(data.corps.erreur); // On affiche le message d'erreur
        } else { // si l'action a bien été effectuée
          var lienModif = "<a href='index.php?page=categories&action=update&id="+data.corps.donnees.id_cat+"' class='modifCat' data-id='"+data.corps.donnees.id_cat+"'>Modifier</a>"; // on recrée le lien vers la modif à partir des données retournées en json
          var lienSuppr = "<a href='index.php?page=categories&action=delete&id="+data.corps.donnees.id_cat+"' class='deleteCat' data-id='"+data.corps.donnees.id_cat+"'>Supprimer</a>"; // on recrée le lien vers la modif à partir des données retournées en json

          if(form.data("mode") == "create") { // si on était sur le formulaire de création
            $(".listeCats ul").append("<li>"+data.corps.donnees.nom_cat+" | "+lienModif+" | "+ lienSuppr +"</li>"); // on ajoute la nouvelle catégorie dans notre liste.
          } else { // si on était sur le formulaire de modification
            // je récupère le nouveau nom de la catégorie
            var nouveauNom = data.corps.donnees.nom_cat;
            form.parent().parent().html(nouveauNom+" | "+lienModif + " | "+lienSuppr);
          }
          form.parent().find("a.closeCat").trigger("click"); // On déclenche l'événement de fermeture du formulaire grâce à la fonction trigger.
        }
      },
      "json") // On précise que la réponse attendue sera au format JSON (sinon les lignes précédentes ne fonctionneront pas).
      .fail(function() {
        console.log( "error" ); // code exécuté en cas d'erreur dans la réponse HTTP.
      })
  });


  $("body").on("click", "a.deleteCat", function(event) {
    event.preventDefault();
    var lien = $(this);
    var item = lien.parent();
    // création de la requête AJAX.
    var url = $(this).attr("href"); // récupération de l'URL de destination.

    $.ajax({
      url: url,
      method: "GET",
      dataType: "json",
      success: function(data) {
        // Fonction utilisée si la requête a bien été éxécutée (mais pas forcément la requête SQL derrière)
        if(data.statut == "reussite") {
          item.html("Catégorie supprimée !");
          setTimeout(function() {
            item.fadeOut()
          }, 1000);
        } else {
          alert("Probleme !");
        }
      },
      error: function() {
        console.log("erreur requête HTTP");
      },
      timeout: function() {
        console.log("requête non aboutie");
      },
      complete: function() {
        console.log("fin de la fonction AJAX");
      }
    });

  });

  // Affichage des infos d'un auteur au survol de son nom, via une requete Ajax
  $("a.ajaxAuteur").mouseover(function(){

    var lien = $(this);
    var posLien = lien.offset();
    var posL = posLien.left;
    var posT = posLien.top;
    var url = $(this).attr("href")+"/ajax";

    $.ajax({
      url: url,
      method: "GET",
      dataType: "html",
      success: function(data) {
        // Fonction utilisée si la requête a bien été éxécutée (mais pas forcément la requête SQL derrière)
        $("body").append(
          "<div class='popAuteur' style='top: "+posT+"; left: "+posL+"'>"+data+"</div>");

      },
      error: function() {
        console.log("erreur requête HTTP");
      },
      timeout: function() {
        console.log("requête non aboutie");
      },
      complete: function() {
        console.log("fin de la fonction AJAX");
      }
    });
  });


  $("a.ajaxAuteur").mouseleave(function(){
    $(".popAuteur").fadeOut();
  });

  // On écoute l'événement "touche relâchée" sur l'élément #searchValue et on appelle une fonction anonyme à laquelle on passe en paramètre l'événement de manière à pouvoir récupérer la touche sur laquelle on a appuyé
  $("#searchValue").keyup(function(event){
    // Si cette touche n'a pas le code 40 (qui correspond à la flèche bas)
    if(event.which != 40) {
      // on stocke la valeur de l'input dans une variable recherche
      var recherche = ($("#searchValue").val());
      // si on a plus de deux caractères
      if(recherche.length > 2) {
        // On lance la requête ajax
        $.ajax({
          // on passe la variable GET["page"]=recherche pour que le système appelle le bon contrôleur
          url: "index.php?page=recherche",
          method: "GET",
          data: {
            // on passe la variable $_GET["mode"]=ajax pour paramétrer la réponse
            "mode": "ajax",
            // on passe la variable $_GET["search"]= la valeur entrée dans l'input
            "search": recherche
          },
          // on définit le type de retour en HTML
          dataType: "html",
          success: function(data) { // en cas de réussite on exécute une fonction anonyme qui prend le retour HTML en paramètre
            // on supprime les liens des suggestions de recherche déjà existantes
            $("#search a").remove();
            // si on a un résultat de recherche qui n'est pas vide
            if(data.length>0) {
              // on l'affiche dans notre moteur après l'input #searchValue
              $("#searchValue").after(data);
            }
            // console.log(data);
          },
          error: function() {
            console.log("erreur requête HTTP");
          },
          timeout: function() {
            console.log("requête non aboutie");
          },
          complete: function() {
            console.log("fin de la fonction AJAX");
          }
        });
      } else { // si l'input contient moins de 3 caractères
        // On enlève les liens des suggestions de recherche déjà existantes
        $("#search a").remove();
      }
    }
  });

  // On écoute l'événement "touche relâchée" sur l'élément #search (qui correspond au formulaire) et on appelle une fonction anonyme à laquelle on passe en paramètre l'événement de manière à pouvoir récupérer la touche sur laquelle on a appuyé
  $("#search").keyup(function(event){
    // si on appuie sur la fleche du bas (code 40)
    if(event.which == 40) {
      // on place le focus sur l'élément a suivant l'élément qui a le focus actuellement
      $(":focus").next("a").focus();
      // si l'élément focus a la classe "res"
      if($(":focus").hasClass("res")) {
        // On récupère sa valeur et on l'injecte dans l'input
        $("#searchValue").val($(":focus").html());
      }
    } else if (event.which == 38) { // si on appuie sur la fleche du haut (code 38)
      // on place le focus sur l'élément soit a, soit input précédant l'élément qui a le focus actuellement
      $(":focus").prev("a, input").focus();
      // si l'élément focus a la classe "res"
      if($(":focus").hasClass("res")) {
        // On récupère sa valeur et on l'injecte dans l'input
        $("#searchValue").val($(":focus").html());
      }
    }
  });


});


/////////////////////// GOOGLE MAPS

var mapStyles = [
  {
    "elementType": "labels",
    "stylers": [
      {
        "color": "#db6094"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape",
    "stylers": [
      {
        "color": "#d78f64"
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "stylers": [
      {
        "color": "#549ae7"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.business",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.local",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "water",
    "stylers": [
      {
        "color": "#50e3eb"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
];

function initialisation() {
  // création d'une google map.
  var map = new google.maps.Map(document.getElementById('mapDiv'), {
    zoom: 4, // niveau de zoom
    maxZoom: 18, // niveau de zoom maximum autorisé
    minZoom: 1, // niveau de zoom minimum autorisé
    center: {lat: 0, lng: 0}, // définition du centre de la carte
    clickableIcons: false, // rend les Points d'intérêt non cliquables
    disableDefaultUI: true, // active/desactive l'interface utilisateur
    fullscreenControl: false, // ajoute le bouton pour passer en fullscreen
    gestureHandling: "auto", // permet de paramétrer ce que l'utilisateur peut faire de la map
    mapTypeControl: false, // affiche ou non les boutons pour changer de type de map (satellite, relief, etc)
    mapTypeId: google.maps.MapTypeId.ROADMAP, // permet de manipuler le type de map qu'on veut afficher (terrain, hybride, roadmap, satellite)
    scrollwheel: true, // Désactive le zoom/dezoom
    streetViewControl: false, // Désactive Pegman
    zoomControl: true, // Active/désactive le zoom
    zoomControlOptions: {position: google.maps.ControlPosition.TOP_LEFT}, // Permet de bouger la position du zoom
    // styles: mapStyles // Permet de styliser la map à travers un objet JSON qu'on peut configurer via https://snazzymaps.com/ ou https://mapstyle.withgoogle.com/
  });


  /////// FONCTION MAP ADMIN
  // Si on est sur l'admin, création d'un marqueur pour positionner l'utilisateur
  if($(".admin").length > 0) {

    // On crée un marqueur, qu'on stocke dans une variable pour pouvoir le manipuler.
    var marker = new google.maps.Marker({
      position: map.getCenter(), // on lui assigne comme position le centre de la map
      map: map, // on l'assigne à notre carte
      draggable: true, // on le rend déplaçable
      title: "Ma position !" // on lui donne un titre
    });

    // Cette fonction va être appelée à la création ou à la récupération des infos
    // Elle permet de placer un nouveau marqueur, de le rendre déplaçable, et de récupérer sa position après déplacement pour l'enregistrer.
    function updateCarte(map, pos) {
      // // On recentre la carte sur les coordonnées de l'utilisateur.
      // équivalent à map.setCenter(pos); mais avec une anim de transition
      map.panTo(pos);
      map.setZoom(14); // map.setZoom(14); // on zoome sur le point qu'on vient de récupérer

      marker.setPosition(pos); // On déplace le marker vers la nouvelle position.

      // On crée un écouteur d'événement sur notre marqueur pour l'événement fin de décplacement
      marker.addListener('dragend', function(){
        var nouvellePosition = marker.getPosition(); // on récupère sa position
        $("#inputLat").val(nouvellePosition.lat); // on injecte la nouvelle latitude dans l'input
        $("#inputLng").val(nouvellePosition.lng); // on injecte la nouvelle longitude dans l'input
        map.panTo(nouvellePosition); // on recentre la map sur la nouvelle position du marqueur
      });
    }

    function codeAddress() {
      // instanciation d'un objet Geocoder
      var geocoder = new google.maps.Geocoder();
      // on récupère l'adresse en concaténant adresse, cp et ville
      var adresse = document.getElementById('adresse').value + " "+ document.getElementById('cp').value + " " + document.getElementById('ville').value;
      // On lance la requête via la méthode geocode
      geocoder.geocode( { 'address': adresse}, function(results, status) {
        if (status == 'OK') {
          // console.log(results);
          var lat = results[0].geometry.location.lat;
          var lng = results[0].geometry.location.lng;
          $("#inputLat").val(lat); // on injecte la nouvelle latitude dans l'input
          $("#inputLng").val(lng); // on injecte la nouvelle longitude dans l'input
          // On récupère les résultats pour mettre à jour la carte via notre fonction updateCarte
          updateCarte(map, results[0].geometry.location);

        } else {
          $("#mapDiv").before("<p>Votre adresse n'a pas pu être récupérée automatiqument.</p>");
        }
      });
    }

    $("#geocodeInfos").focusout(function(){
      codeAddress();
    });

    // si on n'a pas de lat ni de lng renseignée
    if($("#inputLat").val() === "" && $("#inputLng").val() === "") {
      // on récupère la position de l'utilisateur courant et on affiche le marqueur sur la map
      // On essaye de récupérer la localisation de l'utilisateur via l'API intégrée dans le navigateur
       if (navigator.geolocation) { // On teste si le navigateur supporte l'API geolocation
          navigator.geolocation.getCurrentPosition(
            function(position) {
              console.log("lat : "+position.coords.latitude+" / lng : "+position.coords.longitude);
              var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
              };

              // On lance la fonction de création du marqueur et de recentrage de la map
              updateCarte(map, pos);

              $("#inputLat").val(pos.lat);
              $("#inputLng").val(pos.lng);

            }
          );
        //  navigator.geolocation.getCurrentPosition(function(position) {
        //    var pos = {
        //      lat: position.coords.latitude,
        //      lng: position.coords.longitude
        //    };

        //  }, function() {
        //    handleLocationError(true, infoWindow, map.getCenter());
        //  });
       } else {
         // Browser doesn't support Geolocation
         handleLocationError(false, infoWindow, map.getCenter());
       }
    } else {
      // On est dans le cas où on affiche le détail d'un auteur et qu'on a ses coordonnées en base
      var getLat = parseFloat($("#inputLat").val());
      var getLng = parseFloat($("#inputLng").val());
      var pos = { lat: getLat, lng: getLng };

      // On lance la fonction de création du marqueur et de recentrage de la map
      updateCarte(map, pos);
    }
  }

  ///// FONCTION MAP EN FRONT
  if($(".listeUtilisateurs").length > 0) {

    map.setCenter({lat:45, lng: 4.5});

    var markers = [];

    // On boucle sur la liste des utilisateurs présents
    var i = 0;
    $(".utilisateur").each(function(i){
      if($(this).find(".lat").html() !== "" && $(this).find(".lng").html() !== "") {
        var pos = {lat: parseFloat($(this).find(".lat").html()), lng: parseFloat($(this).find(".lng").html())};
        var utilisateur = $(this).find(".nom").html()+" "+$(this).find(".prenom").html();
        var adresse = $(this).find(".adresse").html();
        var infos = "<h4>"+utilisateur+"</h4>"+"<p>"+adresse+"</p>";

        markers[i] = new google.maps.Marker({
          position: pos, // on lui assigne comme position le centre de la map
          map: map, // on l'assigne à notre carte
          title: utilisateur // on lui donne un titre
        });
        markers[i].addListener('click', function() {
          $("#bulle").show();
          $("#bulle .content").html(infos);
        });
      }
      i++;
    });

    console.log(markers);

    var markerCluster = new MarkerClusterer(
      map,
      markers,
      {
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
        gridSize: 20,
        minimumClusterSize: 4
      }
    );



    $("#bulle span").click(function(){
      $("#bulle").hide();
    });
  }

}
