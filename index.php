<?php
// Si besoin, chargement du fichier appConf.php permettant de récupérer les constantes de paramétrage de notre application.
require("appConf.php");
require("modeles/modeleSession.php");

// Chargement de l'autoloader de Composer.
// L'autoloader va permetter d'automatiquement charger les classes PHP utilisées dans le code, sans avoir à faire les requires habituels
require BASE_URL.'/vendor/autoload.php';


session_start();

// L'index du site va servir de contrôleur frontal ou front controller en anglais.
// Son rôle est de traiter la requête HTTP demandée par le client et d'appeler le controleur spécifique correspondant.
// Pour ça on utilise une fonction (ou un ensemble de fonctions) qu'on appelle routeur.

// Déclaration de la fonction du routeur
function routeur() {

  // on crée un tableau contenant les entités qui seront soumises à un contrôle par l'administration
  $tabAdmin = array(
    "Utilisateurs",
    "Actualites",
    "Illustrations",
    "IllusActus"
  );

  // on regarde si on a une variable $_GET["page"]
  if(!empty($_GET["page"])) {
    // on sécurise $_GET["page"] et on passe sa première lettre en majuscule
    $page = ucfirst(htmlspecialchars($_GET["page"]));

    // On teste si $page est une entité publique ou privée
    if(in_array($page, $tabAdmin)) {
      verifSession();
    }

    // on recompose le chemin d'accès au contrôleur
    $chemin = "controleurs/controleur".$page.".php";
    // On vérifie que le fichier du contrôleur existe, et on le charge.
    if(file_exists($chemin)) {
      require($chemin);
    } else {
      // si le fichier du controleur n'existe pas, on redirige vers la 404.php
      require("404.php");
    }
  } else {
    // Chargement de la page d'accueil
    $chemin = "controleurs/controleurAccueil.php";
    require($chemin);
  }
}

// appel du routeur
routeur();
